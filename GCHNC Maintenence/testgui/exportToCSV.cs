﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace MaintenenceGUI
{
    class exportToCSV
    {
        private static string dir = Application.StartupPath + @"\database\temp\";

        public void createTasksCSVDocument()
        {
            try
            {
                string cnStr =
           "Provider=Microsoft.Jet.OLEDB.4.0;" +
           "Extended Properties='text;HDR=Yes;FMT=Delimited';" +
           "Data Source=" + dir + ";";

                using (var cn = new OleDbConnection(cnStr))
                {
                    cn.Open();
                    var cmd = new OleDbCommand(
                                "CREATE TABLE data.csv ([Date] VARCHAR(30), [Task] VARCHAR(30), [Completed] VARCHAR(5), [Frequency] VARCHAR(10))", cn);

                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public void generateTasksCSVDocument(string fileName, string date, string element)
        {
            try
            {
                string cnStr =
           "Provider=Microsoft.Jet.OLEDB.4.0;" +
           "Extended Properties='text;HDR=Yes;FMT=Delimited';" +
           "Data Source=" + dir + ";";


                using (var cn = new OleDbConnection(cnStr))
                {
                    cn.Open();

                    //define the file layout (a.k.a. the table)  


                    //start pumping data  


                    XmlDocument xdoc = new XmlDocument();
                    FileStream rfile = new FileStream(fileName, FileMode.Open);
                    xdoc.Load(rfile);

                    XmlNodeList list = xdoc.GetElementsByTagName(element);
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (element == "tasks")
                        {
                            var cmd = new OleDbCommand(
                        "INSERT INTO data.csv ([Date], [Task], [Completed], [Frequency]) VALUES (?, ?, ?, ?)", cn);
                            XmlElement task = (XmlElement)xdoc.GetElementsByTagName("task")[i];
                            XmlElement completed = (XmlElement)xdoc.GetElementsByTagName("completed")[i];
                           
                            cmd.Parameters.AddWithValue("?", date);
                            cmd.Parameters.AddWithValue("?", task.InnerText + "");
                            cmd.Parameters.AddWithValue("?", completed.InnerText + "");
                            cmd.Parameters.AddWithValue("", "daily task");
                            cmd.ExecuteNonQuery();

                        }
                        else if (element == "extra_tasks")
                        {
                            var cmd = new OleDbCommand(
                        "INSERT INTO data.csv ([Date], [Task], [Completed], [Frequency]) VALUES (?, ?, ?, ?)", cn);
                            XmlElement task = (XmlElement)xdoc.GetElementsByTagName("task")[i];
                            XmlElement completed = (XmlElement)xdoc.GetElementsByTagName("completed")[i];
                            XmlElement frequency = (XmlElement)xdoc.GetElementsByTagName("weekly_or_monthly")[i];
                            cmd.Parameters.AddWithValue("?", date);
                            cmd.Parameters.AddWithValue("?", task.InnerText + "");
                            cmd.Parameters.AddWithValue("?", completed.InnerText + "");
                            cmd.Parameters.AddWithValue("?", frequency.InnerText + " task");
                            cmd.ExecuteNonQuery();
                        }
                        else { }
                    }
                    rfile.Close();
                    cn.Close();

                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }
        public void createContractorsCSVDocument()
        {
            try
            {
                string cnStr =
           "Provider=Microsoft.Jet.OLEDB.4.0;" +
           "Extended Properties='text;HDR=Yes;FMT=Delimited';" +
           "Data Source=" + dir + ";";

                using (var cn = new OleDbConnection(cnStr))
                {
                    cn.Open();
                    var cmd = new OleDbCommand(
                                "CREATE TABLE contractors.csv ([Name] VARCHAR(30), [Company] VARCHAR(30), [Description] VARCHAR(100), [Date] VARCHAR(10))", cn);

                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public void generateContractorsCSVDocument(string fileName, string date, string element)
        {
            try
            {
                string cnStr =
           "Provider=Microsoft.Jet.OLEDB.4.0;" +
           "Extended Properties='text;HDR=Yes;FMT=Delimited';" +
           "Data Source=" + dir + ";";


                using (var cn = new OleDbConnection(cnStr))
                {
                    cn.Open();

                    //define the file layout (a.k.a. the table)  


                    //start pumping data  


                    XmlDocument xdoc = new XmlDocument();
                    FileStream rfile = new FileStream(fileName, FileMode.Open);
                    xdoc.Load(rfile);

                    XmlNodeList list = xdoc.GetElementsByTagName(element);
                    for (int i = 0; i < list.Count; i++)
                    {
                            var cmd = new OleDbCommand(
                        "INSERT INTO contractors.csv ([Name], [Company], [Description], [Date]) VALUES (?, ?, ?, ?)", cn);
                            XmlElement name = (XmlElement)xdoc.GetElementsByTagName("name")[i];
                            XmlElement company = (XmlElement)xdoc.GetElementsByTagName("company")[i];
                            XmlElement description = (XmlElement)xdoc.GetElementsByTagName("description")[i];
                            XmlElement dates = (XmlElement)xdoc.GetElementsByTagName("date")[i];
                            cmd.Parameters.AddWithValue("?", name.InnerText);
                            cmd.Parameters.AddWithValue("?", company.InnerText + "");
                            cmd.Parameters.AddWithValue("?", description.InnerText + "");
                            cmd.Parameters.AddWithValue("?", dates.InnerText);
                            cmd.ExecuteNonQuery();
                    }
                    rfile.Close();
                    cn.Close();

                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public void createProvidersCSVDocument()
        {
            try
            {
                string cnStr =
           "Provider=Microsoft.Jet.OLEDB.4.0;" +
           "Extended Properties='text;HDR=Yes;FMT=Delimited';" +
           "Data Source=" + dir + ";";

                using (var cn = new OleDbConnection(cnStr))
                {
                    cn.Open();
                    var cmd = new OleDbCommand(
                                "CREATE TABLE providers.csv ([Name] VARCHAR(30), [Date Due] VARCHAR(30), [Date Completed] VARCHAR(30))", cn);

                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public void generateProvidersCSVDocument(string fileName, string date, string element)
        {
            try
            {
                string cnStr =
           "Provider=Microsoft.Jet.OLEDB.4.0;" +
           "Extended Properties='text;HDR=Yes;FMT=Delimited';" +
           "Data Source=" + dir + ";";

                using (var cn = new OleDbConnection(cnStr))
                {
                    cn.Open();

                    //define the file layout (a.k.a. the table)  
                    //start pumping data  

                    XmlDocument xdoc = new XmlDocument();
                    FileStream rfile = new FileStream(fileName, FileMode.Open);
                    xdoc.Load(rfile);

                    XmlNodeList list = xdoc.GetElementsByTagName(element);
                    for (int i = 0; i < list.Count; i++)
                    {
                        var cmd = new OleDbCommand(
                    "INSERT INTO providers.csv ([Name], [Date Due], [Date Completed]) VALUES (?, ?, ?)", cn);
                        XmlElement name = (XmlElement)xdoc.GetElementsByTagName("name")[i];
                        XmlElement due = (XmlElement)xdoc.GetElementsByTagName("due_date")[i];
                        XmlElement completed = (XmlElement)xdoc.GetElementsByTagName("date_completed")[i];
                        
                        cmd.Parameters.AddWithValue("?", name.InnerText);
                        cmd.Parameters.AddWithValue("?", due.InnerText + "");
                        cmd.Parameters.AddWithValue("?", completed.InnerText + "");
                        
                        cmd.ExecuteNonQuery();
                    }
                    rfile.Close();
                    cn.Close();

                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public void createMaintenenceCSVDocument()
        {
            try
            {
                string cnStr =
           "Provider=Microsoft.Jet.OLEDB.4.0;" +
           "Extended Properties='text;HDR=Yes;FMT=Delimited';" +
           "Data Source=" + dir + ";";

                using (var cn = new OleDbConnection(cnStr))
                {
                    cn.Open();
                    var cmd = new OleDbCommand(
                                "CREATE TABLE maintenence.csv ([Item] VARCHAR (40), [Area] VARCHAR(30), [Room] VARCHAR(30), [Description] VARCHAR(100), [Date] VARCHAR(10))", cn);

                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public void createInventoryCSVDocument()
        {
            try
            {
                string cnStr =
           "Provider=Microsoft.Jet.OLEDB.4.0;" +
           "Extended Properties='text;HDR=Yes;FMT=Delimited';" +
           "Data Source=" + dir + ";";

                using (var cn = new OleDbConnection(cnStr))
                {
                    cn.Open();
                    var cmd = new OleDbCommand(
                                "CREATE TABLE inventory.csv ([Item] VARCHAR (40), [Area] VARCHAR(30), [Quantity] VARCHAR(30))", cn);

                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public void generateInventoryCSVDocument(string fileName, string date, string element)
        {
            try
            {
                string cnStr =
           "Provider=Microsoft.Jet.OLEDB.4.0;" +
           "Extended Properties='text;HDR=Yes;FMT=Delimited';" +
           "Data Source=" + dir + ";";


                using (var cn = new OleDbConnection(cnStr))
                {
                    cn.Open();

                    //define the file layout (a.k.a. the table)  


                    //start pumping data  


                    XmlDocument xdoc = new XmlDocument();
                    FileStream rfile = new FileStream(fileName, FileMode.Open);
                    xdoc.Load(rfile);

                    XmlNodeList list = xdoc.GetElementsByTagName(element);
                    for (int i = 0; i < list.Count; i++)
                    {
                        var cmd = new OleDbCommand(
                    "INSERT INTO inventory.csv ([Item], [Area], [Quantity]) VALUES (?, ?, ?)", cn);
                        XmlElement item = (XmlElement)xdoc.GetElementsByTagName("item")[i];
                        XmlElement area = (XmlElement)xdoc.GetElementsByTagName("area")[i];
                        XmlElement quant = (XmlElement)xdoc.GetElementsByTagName("quantity")[i];
                        cmd.Parameters.AddWithValue("?", item.InnerText);
                        cmd.Parameters.AddWithValue("?", area.InnerText);
                        cmd.Parameters.AddWithValue("?", quant.InnerText + "");
                        cmd.ExecuteNonQuery();
                    }
                    rfile.Close();
                    cn.Close();

                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public void generateMaintenenceCSVDocument(string fileName, string date, string element)
        {
            try
            {
                string cnStr =
           "Provider=Microsoft.Jet.OLEDB.4.0;" +
           "Extended Properties='text;HDR=Yes;FMT=Delimited';" +
           "Data Source=" + dir + ";";


                using (var cn = new OleDbConnection(cnStr))
                {
                    cn.Open();

                    //define the file layout (a.k.a. the table)  


                    //start pumping data  


                    XmlDocument xdoc = new XmlDocument();
                    FileStream rfile = new FileStream(fileName, FileMode.Open);
                    xdoc.Load(rfile);

                    XmlNodeList list = xdoc.GetElementsByTagName(element);
                    for (int i = 0; i < list.Count; i++)
                    {
                        var cmd = new OleDbCommand(
                    "INSERT INTO maintenence.csv ([Item], [Area], [Quantity]) VALUES (?, ?, ?)", cn);
                        XmlElement item = (XmlElement)xdoc.GetElementsByTagName("item")[i];
                        XmlElement area = (XmlElement)xdoc.GetElementsByTagName("area")[i];
                        XmlElement quantity = (XmlElement)xdoc.GetElementsByTagName("quantity")[i];
                        cmd.Parameters.AddWithValue("?", item.InnerText);
                        cmd.Parameters.AddWithValue("?", area.InnerText);
                        cmd.Parameters.AddWithValue("?", quantity.InnerText + "");
                        cmd.ExecuteNonQuery();
                    }
                    rfile.Close();
                    cn.Close();

                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public void createContractorListsCSVDocument()
        {
            try
            {
                string cnStr =
           "Provider=Microsoft.Jet.OLEDB.4.0;" +
           "Extended Properties='text;HDR=Yes;FMT=Delimited';" +
           "Data Source=" + dir + ";";

                using (var cn = new OleDbConnection(cnStr))
                {
                    cn.Open();
                    var cmd = new OleDbCommand(
                                "CREATE TABLE contractorlists.csv ([Name] VARCHAR(30), [Company] VARCHAR(30),[Phone] VARCHAR(20), [Description] VARCHAR(100), [NextVist] VARCHAR(15), [CertNo] VARCHAR(40), [Date] VARCHAR(30))", cn);

                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public void generateContractorListsCSVDocument(string fileName, string date, string element)
        {
            try
            {
                string cnStr =
           "Provider=Microsoft.Jet.OLEDB.4.0;" +
           "Extended Properties='text;HDR=Yes;FMT=Delimited';" +
           "Data Source=" + dir + ";";


                using (var cn = new OleDbConnection(cnStr))
                {
                    cn.Open();

                    //define the file layout (a.k.a. the table)  


                    //start pumping data  


                    XmlDocument xdoc = new XmlDocument();
                    FileStream rfile = new FileStream(fileName, FileMode.Open);
                    xdoc.Load(rfile);

                    XmlNodeList list = xdoc.GetElementsByTagName(element);
                    for (int i = 0; i < list.Count; i++)
                    {
                                   
                        var cmd = new OleDbCommand(
                    "INSERT INTO contractorlists.csv ([Name], [Company], [Phone], [Description], [NextVist], [CertNo], [Date]) VALUES (?, ?, ?, ?, ?, ?, ?)", cn);
                        XmlElement name = (XmlElement)xdoc.GetElementsByTagName("name")[i];
                        XmlElement company = (XmlElement)xdoc.GetElementsByTagName("company")[i];
                        XmlElement phone = (XmlElement)xdoc.GetElementsByTagName("phone")[i];
                        XmlElement description = (XmlElement)xdoc.GetElementsByTagName("description")[i];
                        XmlElement visit = (XmlElement)xdoc.GetElementsByTagName("next_visit")[i];
                        XmlElement cert = (XmlElement)xdoc.GetElementsByTagName("certification_no")[i];
                        
                        cmd.Parameters.AddWithValue("?", name.InnerText);
                        cmd.Parameters.AddWithValue("?", company.InnerText + "");
                        cmd.Parameters.AddWithValue("?", phone.InnerText + "");
                        cmd.Parameters.AddWithValue("?", description.InnerText + "");
                        cmd.Parameters.AddWithValue("?", visit.InnerText + "");
                        cmd.Parameters.AddWithValue("?", cert.InnerText + "");
                        cmd.Parameters.AddWithValue("?", DateTime.Now.ToString());
                        cmd.ExecuteNonQuery();
                        


                    }
                    rfile.Close();
                    cn.Close();

                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

 
    }
}
