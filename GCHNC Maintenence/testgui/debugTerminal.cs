﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MaintenenceGUI
{
    public partial class debugTerminal : Form
    {
        public debugTerminal()
        {
            InitializeComponent();
            richTextBox1.ReadOnly = true;
            richTextBox1.Text = "";
        }

        public void output(string error){
            
            richTextBox1.Text += error;
        
        }

    }
}
