﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MaintenenceGUI
{
    class fileFolder
    {
        public void createFolder(string name)
        {
            //creates a folder in the stated directory

            //try to create folder
            if (!System.IO.Directory.Exists(name))
            {
                //create folder
                try
                {
                    System.IO.Directory.CreateDirectory(name);

                }
                catch (Exception err)
                {
                    //display error message to string
                }
            }
        }

        public bool createOrCheckFile(string name, bool newFile)
        {//here we can create a file for writing - used for caching or logs ONLY
            //this method is NOT USED to create the xml databases - dataBase -> create()
                //is used for this
                    

            //check if file already exists
            if (!System.IO.File.Exists(name))
            {
                //if not create it
                if (newFile)
                {
                    try
                    {
                        System.IO.FileStream fs = System.IO.File.Create(name);

                    }
                    catch (Exception err)
                    {//display error message to string
                    }
                }
                return true;
            
            }
            else
            { return false; }
            }

        public void directoryCopy(string source, string dest, bool copySubDirs)
    {
        // Get the subdirectories for the specified directory.
        DirectoryInfo dir = new DirectoryInfo(source);
        DirectoryInfo[] dirs = dir.GetDirectories();

        if (!dir.Exists)
        {
            throw new DirectoryNotFoundException(
                "Source directory does not exist or could not be found: "
                + source);
        }

        // If the destination directory doesn't exist, create it. 
        if (!Directory.Exists(dest))
        {
            Directory.CreateDirectory(dest);
        }

        // Get the files in the directory and copy them to the new location.
        FileInfo[] files = dir.GetFiles();
        foreach (FileInfo file in files)
        {
            string temppath = Path.Combine(dest, file.Name);
            file.CopyTo(temppath, false);
        }

        // If copying subdirectories, copy them and their contents to new location. 
        if (copySubDirs)
        {
            foreach (DirectoryInfo subdir in dirs)
            {
                string temppath = Path.Combine(dest, subdir.Name);
                directoryCopy(subdir.FullName, temppath, copySubDirs);
            }
        }
    }
}
        }
    
