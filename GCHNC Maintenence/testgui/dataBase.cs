﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;


namespace MaintenenceGUI
{
    class dataBase
    {
        public dataBase() { }

        
        public bool create(string xmlRoot, string fileName)
        {//create the root elements of the xml document if the dont alreasy exist
            try
            {
                if (!System.IO.File.Exists(fileName))
                {
                    XmlTextWriter xtw;
                    xtw = new XmlTextWriter(fileName, Encoding.UTF8);
                    xtw.WriteStartDocument();
                    xtw.WriteStartElement(xmlRoot);
                    xtw.WriteEndElement();
                    xtw.Close();

                    //if file exists return true
                    return true;
                }
                else { return false; }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString()); 
                return false;
            }
        }

        public void write(string fileName, ArrayList xmlElement,ArrayList xmlText, string rootElementName)
        {// this method dynamicly writes xml based on the two arraylist parameters
            XmlDocument xd = new XmlDocument();
            
            FileStream lfile = new FileStream(fileName, FileMode.Open);
            xd.Load(lfile);
                                                      
            XmlElement cl = xd.CreateElement(rootElementName);
            for(int i = 0; i < xmlText.Count; i++){
              XmlElement element = xd.CreateElement(xmlElement[i].ToString());
              //create text
              XmlText text = xd.CreateTextNode(xmlText[i].ToString());
                            
              element.AppendChild(text);
              cl.AppendChild(element);
              xd.DocumentElement.AppendChild(cl); 
        }
              
            lfile.Close();
            xd.Save(fileName);
        }

        public void delete(string fileName, string xmlNode) 
        {
            FileStream rfile = new FileStream(fileName, FileMode.Open);
            XmlDocument tdoc = new XmlDocument();
            tdoc.Load(rfile);

            XmlNode node = tdoc.SelectSingleNode(xmlNode);  
            node.ParentNode.RemoveChild(node);
            
            rfile.Close();
            tdoc.Save(fileName);
        }

 
    }
}