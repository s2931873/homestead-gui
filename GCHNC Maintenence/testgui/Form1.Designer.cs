﻿namespace MaintenenceGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.imageListNavIcons = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStripLeftNav = new System.Windows.Forms.MenuStrip();
            this.toolStripHeader = new System.Windows.Forms.ToolStripMenuItem();
            this.taskListToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.todaysTasksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.weeklyTasksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.manageTasksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maintenenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryMaintenenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryStockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemLegilativeMaintenance = new System.Windows.Forms.ToolStripMenuItem();
            this.contractorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contractorVisitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.manageContractorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveListsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageDashboard = new System.Windows.Forms.TabPage();
            this.richTextBoxDashTasks = new System.Windows.Forms.RichTextBox();
            this.tabPageTaskList = new System.Windows.Forms.TabPage();
            this.TasksSelection = new System.Windows.Forms.TabControl();
            this.tabPageChooseTodaysTasks = new System.Windows.Forms.TabPage();
            this.buttonNoDailyTasks = new System.Windows.Forms.Button();
            this.labelNoDailyItems = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.checkBoxAddOneOffTask = new System.Windows.Forms.CheckBox();
            this.labelAddOneOffTaskValidate = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.richTextBoxTodaysTaskAddDescrition = new System.Windows.Forms.RichTextBox();
            this.buttonTodaysTasksAddExtraTask = new System.Windows.Forms.Button();
            this.labelTodaysTasksAddExtraTashHeader = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelTodaysTasksExtraTaskName = new System.Windows.Forms.Label();
            this.textBoxTodaysTasksExtraTask = new System.Windows.Forms.TextBox();
            this.labelTodaysTasksUpdate = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.buttonTodaysTasksUpdate = new System.Windows.Forms.Button();
            this.labelTodaysDate = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.task = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaskDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.completed = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageChooseWeeklyTasks = new System.Windows.Forms.TabPage();
            this.buttonExtraTasksUpdate = new System.Windows.Forms.Button();
            this.labelExtraTasksDate = new System.Windows.Forms.Label();
            this.labelExtraTasksHeader = new System.Windows.Forms.Label();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weekly_or_monthly = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.date_completed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageChooseAddTasks = new System.Windows.Forms.TabPage();
            this.buttonAddTasksUpdate = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radioButtonSixMonthly = new System.Windows.Forms.RadioButton();
            this.radioButtonThreeMonthly = new System.Windows.Forms.RadioButton();
            this.radioButtonTwoMonthly = new System.Windows.Forms.RadioButton();
            this.buttonFutureTaskAdd = new System.Windows.Forms.Button();
            this.labelFutureTasksHeader1 = new System.Windows.Forms.Label();
            this.labelAddTaskValidate = new System.Windows.Forms.Label();
            this.labelFutureTaskName = new System.Windows.Forms.Label();
            this.radioButtonDailyTask = new System.Windows.Forms.RadioButton();
            this.textBoxFutureName = new System.Windows.Forms.TextBox();
            this.radioButtonWeeklyTask = new System.Windows.Forms.RadioButton();
            this.radioButtonMonthlyTask = new System.Windows.Forms.RadioButton();
            this.labelFutureTasksHeader = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.task_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.frequency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelTodaysTasksFileNotFound = new System.Windows.Forms.Label();
            this.tabPageInventoryMaintenence = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPageItemMaintenence = new System.Windows.Forms.TabPage();
            this.buttonNoMaintenenceItems = new System.Windows.Forms.Button();
            this.labelMaintenenceNoTasks = new System.Windows.Forms.Label();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.button3 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonAddItemMaintenence = new System.Windows.Forms.Button();
            this.richTextBoxAddItemMaintenceDescription = new System.Windows.Forms.RichTextBox();
            this.textBoxItemMaintenenceArea = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxAddItemMaintenenceItem = new System.Windows.Forms.TextBox();
            this.textBoxAddItemMaintenceRoom = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.Item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Room = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageInventory = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.buttonUpdateInventory = new System.Windows.Forms.Button();
            this.labelAddInventoryValidate = new System.Windows.Forms.Label();
            this.textBoxAddInventoryArea = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.textBoxAddInventoryQuantity = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.buttonAddInventroyItem = new System.Windows.Forms.Button();
            this.textBoxAddInventoryItem = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelExtraTasksUpdate = new System.Windows.Forms.Label();
            this.tabPageMaintenanceLegislation = new System.Windows.Forms.TabPage();
            this.labelAddProviderValidate = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.buttonAddServiceProvider = new System.Windows.Forms.Button();
            this.textBoxAddProviderDateCompleted = new System.Windows.Forms.TextBox();
            this.textBoxAddProviderDueDate = new System.Windows.Forms.TextBox();
            this.textBoxAddProvidersName = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.dataGridView8 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date_completed1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date_due1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonUpdateServiceProviders = new System.Windows.Forms.Button();
            this.tabPageContractors = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPageThisMonthsContractors = new System.Windows.Forms.TabPage();
            this.buttonNoContractors = new System.Windows.Forms.Button();
            this.labelContractorsNoTasks = new System.Windows.Forms.Label();
            this.panelDisplayAddContractorVisit = new System.Windows.Forms.Panel();
            this.buttonAddDailyContractorVisit = new System.Windows.Forms.Button();
            this.richTextBoxAddTodaysContractorDescription = new System.Windows.Forms.RichTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxAddDailyContractorName = new System.Windows.Forms.TextBox();
            this.labelAddDailyContractorName = new System.Windows.Forms.Label();
            this.labelUpdateTodaysContractors = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.buttonTodaysContractorsUpdate = new System.Windows.Forms.Button();
            this.labelSelectVisitingContractor = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VisitDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageAddContractors = new System.Windows.Forms.TabPage();
            this.labelUpdateContractors = new System.Windows.Forms.Label();
            this.buttonuupdateContractors = new System.Windows.Forms.Button();
            this.labelAddContractorsHeader = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.comboBoxAddContractorPeriodOfTime = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBoxAddContractorAmountOfTimeBetweenVisits = new System.Windows.Forms.TextBox();
            this.checkBoxAddContractorEveryXMonths = new System.Windows.Forms.CheckBox();
            this.textBoxAddContractorPhone = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBoxAddContractorCertNumber = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBoxAddContractorNextVisit = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.labelAddContractorValidate = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.richTextBoxAddContractorDescription = new System.Windows.Forms.RichTextBox();
            this.textBoxAddContractorCompany = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonAddContractor = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxAddContractorName = new System.Windows.Forms.TextBox();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.company = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Certification_Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vistitFrequency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelAddTasksUpdate = new System.Windows.Forms.Label();
            this.tabPageExportLists = new System.Windows.Forms.TabPage();
            this.SaveSelection = new System.Windows.Forms.TabControl();
            this.tabPageSaveToExcel = new System.Windows.Forms.TabPage();
            this.buttonDebug = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.comboBoxChooseSaveList = new System.Windows.Forms.ComboBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.exportCSVValidate = new System.Windows.Forms.Label();
            this.buttonSaveToCSV = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxSaveToCSVEndYear = new System.Windows.Forms.TextBox();
            this.textBoxSaveToCSVStartYear = new System.Windows.Forms.TextBox();
            this.textBoxSaveToCSVEndDay = new System.Windows.Forms.TextBox();
            this.textBoxSaveToCSVStartDate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxSaveToCSVEndMonth = new System.Windows.Forms.ComboBox();
            this.comboBoxSaveToCSVStartMonth = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.menuStripLeftNav.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageDashboard.SuspendLayout();
            this.tabPageTaskList.SuspendLayout();
            this.TasksSelection.SuspendLayout();
            this.tabPageChooseTodaysTasks.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPageChooseWeeklyTasks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabPageChooseAddTasks.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPageInventoryMaintenence.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPageItemMaintenence.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.tabPageInventory.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            this.tabPageMaintenanceLegislation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).BeginInit();
            this.tabPageContractors.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPageThisMonthsContractors.SuspendLayout();
            this.panelDisplayAddContractorVisit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.tabPageAddContractors.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.tabPageExportLists.SuspendLayout();
            this.SaveSelection.SuspendLayout();
            this.tabPageSaveToExcel.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageListNavIcons
            // 
            this.imageListNavIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListNavIcons.ImageStream")));
            this.imageListNavIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListNavIcons.Images.SetKeyName(0, "home48.png");
            this.imageListNavIcons.Images.SetKeyName(1, "tasklist.png");
            this.imageListNavIcons.Images.SetKeyName(2, "maintenance.png");
            this.imageListNavIcons.Images.SetKeyName(3, "legislation.png");
            this.imageListNavIcons.Images.SetKeyName(4, "contractor.png");
            this.imageListNavIcons.Images.SetKeyName(5, "save.png");
            this.imageListNavIcons.Images.SetKeyName(6, "settings.png");
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoScrollMinSize = new System.Drawing.Size(0, 750);
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::MaintenenceGUI.Properties.Resources.tweed;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.menuStripLeftNav);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Location = new System.Drawing.Point(2, -7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(2000, 1000);
            this.panel1.TabIndex = 1;
            // 
            // menuStripLeftNav
            // 
            this.menuStripLeftNav.AutoSize = false;
            this.menuStripLeftNav.BackColor = System.Drawing.Color.SlateGray;
            this.menuStripLeftNav.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStripLeftNav.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStripLeftNav.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripHeader,
            this.taskListToolStripMenuItem1,
            this.maintenenceToolStripMenuItem,
            this.toolStripMenuItemLegilativeMaintenance,
            this.contractorsToolStripMenuItem,
            this.saveListsToolStripMenuItem});
            this.menuStripLeftNav.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.menuStripLeftNav.Location = new System.Drawing.Point(0, 62);
            this.menuStripLeftNav.Name = "menuStripLeftNav";
            this.menuStripLeftNav.Size = new System.Drawing.Size(116, 236);
            this.menuStripLeftNav.TabIndex = 2;
            this.menuStripLeftNav.Text = "menuStrip2";
            // 
            // toolStripHeader
            // 
            this.toolStripHeader.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripHeader.ForeColor = System.Drawing.Color.Black;
            this.toolStripHeader.Name = "toolStripHeader";
            this.toolStripHeader.Size = new System.Drawing.Size(109, 24);
            this.toolStripHeader.Text = "Navigation";
            // 
            // taskListToolStripMenuItem1
            // 
            this.taskListToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.todaysTasksToolStripMenuItem,
            this.weeklyTasksToolStripMenuItem,
            this.toolStripSeparator3,
            this.manageTasksToolStripMenuItem});
            this.taskListToolStripMenuItem1.ForeColor = System.Drawing.Color.Black;
            this.taskListToolStripMenuItem1.Name = "taskListToolStripMenuItem1";
            this.taskListToolStripMenuItem1.Size = new System.Drawing.Size(109, 24);
            this.taskListToolStripMenuItem1.Text = "Task List";
            // 
            // todaysTasksToolStripMenuItem
            // 
            this.todaysTasksToolStripMenuItem.Name = "todaysTasksToolStripMenuItem";
            this.todaysTasksToolStripMenuItem.Size = new System.Drawing.Size(171, 24);
            this.todaysTasksToolStripMenuItem.Text = "Todays Tasks";
            this.todaysTasksToolStripMenuItem.Click += new System.EventHandler(this.leftNavTodaysTasks);
            // 
            // weeklyTasksToolStripMenuItem
            // 
            this.weeklyTasksToolStripMenuItem.Name = "weeklyTasksToolStripMenuItem";
            this.weeklyTasksToolStripMenuItem.Size = new System.Drawing.Size(171, 24);
            this.weeklyTasksToolStripMenuItem.Text = "Extra Tasks";
            this.weeklyTasksToolStripMenuItem.Click += new System.EventHandler(this.leftNavWeeklyTasks);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(168, 6);
            // 
            // manageTasksToolStripMenuItem
            // 
            this.manageTasksToolStripMenuItem.Name = "manageTasksToolStripMenuItem";
            this.manageTasksToolStripMenuItem.Size = new System.Drawing.Size(171, 24);
            this.manageTasksToolStripMenuItem.Text = "Manage Tasks";
            this.manageTasksToolStripMenuItem.Click += new System.EventHandler(this.leftNavAddTasks);
            // 
            // maintenenceToolStripMenuItem
            // 
            this.maintenenceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inventoryMaintenenceToolStripMenuItem,
            this.inventoryStockToolStripMenuItem});
            this.maintenenceToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.maintenenceToolStripMenuItem.Name = "maintenenceToolStripMenuItem";
            this.maintenenceToolStripMenuItem.Size = new System.Drawing.Size(109, 24);
            this.maintenenceToolStripMenuItem.Text = "Maintenence";
            // 
            // inventoryMaintenenceToolStripMenuItem
            // 
            this.inventoryMaintenenceToolStripMenuItem.Name = "inventoryMaintenenceToolStripMenuItem";
            this.inventoryMaintenenceToolStripMenuItem.Size = new System.Drawing.Size(228, 24);
            this.inventoryMaintenenceToolStripMenuItem.Text = "Inventory Maintenence";
            this.inventoryMaintenenceToolStripMenuItem.Click += new System.EventHandler(this.leftNavInventoryMaintenence);
            // 
            // inventoryStockToolStripMenuItem
            // 
            this.inventoryStockToolStripMenuItem.Name = "inventoryStockToolStripMenuItem";
            this.inventoryStockToolStripMenuItem.Size = new System.Drawing.Size(228, 24);
            this.inventoryStockToolStripMenuItem.Text = "Inventory Stock";
            this.inventoryStockToolStripMenuItem.Click += new System.EventHandler(this.leftNavInventoryStock);
            // 
            // toolStripMenuItemLegilativeMaintenance
            // 
            this.toolStripMenuItemLegilativeMaintenance.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItemLegilativeMaintenance.Name = "toolStripMenuItemLegilativeMaintenance";
            this.toolStripMenuItemLegilativeMaintenance.Size = new System.Drawing.Size(109, 24);
            this.toolStripMenuItemLegilativeMaintenance.Text = "Legislation";
            this.toolStripMenuItemLegilativeMaintenance.Click += new System.EventHandler(this.leftNavLegislative);
            // 
            // contractorsToolStripMenuItem
            // 
            this.contractorsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contractorVisitToolStripMenuItem,
            this.toolStripSeparator8,
            this.manageContractorsToolStripMenuItem});
            this.contractorsToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.contractorsToolStripMenuItem.Name = "contractorsToolStripMenuItem";
            this.contractorsToolStripMenuItem.Size = new System.Drawing.Size(109, 24);
            this.contractorsToolStripMenuItem.Text = "Contractors";
            // 
            // contractorVisitToolStripMenuItem
            // 
            this.contractorVisitToolStripMenuItem.Name = "contractorVisitToolStripMenuItem";
            this.contractorVisitToolStripMenuItem.Size = new System.Drawing.Size(212, 24);
            this.contractorVisitToolStripMenuItem.Text = "Contractor Visit";
            this.contractorVisitToolStripMenuItem.Click += new System.EventHandler(this.leftNavContractorVisit);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(209, 6);
            // 
            // manageContractorsToolStripMenuItem
            // 
            this.manageContractorsToolStripMenuItem.Name = "manageContractorsToolStripMenuItem";
            this.manageContractorsToolStripMenuItem.Size = new System.Drawing.Size(212, 24);
            this.manageContractorsToolStripMenuItem.Text = "Manage Contractors";
            this.manageContractorsToolStripMenuItem.Click += new System.EventHandler(this.leftNavManageContractors);
            // 
            // saveListsToolStripMenuItem
            // 
            this.saveListsToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.saveListsToolStripMenuItem.Name = "saveListsToolStripMenuItem";
            this.saveListsToolStripMenuItem.Size = new System.Drawing.Size(109, 24);
            this.saveListsToolStripMenuItem.Text = "Save Lists";
            this.saveListsToolStripMenuItem.Click += new System.EventHandler(this.leftNavSaveTaskLists);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageDashboard);
            this.tabControl1.Controls.Add(this.tabPageTaskList);
            this.tabControl1.Controls.Add(this.tabPageInventoryMaintenence);
            this.tabControl1.Controls.Add(this.tabPageMaintenanceLegislation);
            this.tabControl1.Controls.Add(this.tabPageContractors);
            this.tabControl1.Controls.Add(this.tabPageExportLists);
            this.tabControl1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.HotTrack = true;
            this.tabControl1.ImageList = this.imageListNavIcons;
            this.tabControl1.ItemSize = new System.Drawing.Size(50, 50);
            this.tabControl1.Location = new System.Drawing.Point(115, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1183, 780);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPageDashboard
            // 
            this.tabPageDashboard.Controls.Add(this.richTextBoxDashTasks);
            this.tabPageDashboard.ImageIndex = 0;
            this.tabPageDashboard.Location = new System.Drawing.Point(4, 54);
            this.tabPageDashboard.Name = "tabPageDashboard";
            this.tabPageDashboard.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDashboard.Size = new System.Drawing.Size(1175, 722);
            this.tabPageDashboard.TabIndex = 6;
            this.tabPageDashboard.Text = "Dashboard";
            this.tabPageDashboard.UseVisualStyleBackColor = true;
            // 
            // richTextBoxDashTasks
            // 
            this.richTextBoxDashTasks.Location = new System.Drawing.Point(11, 6);
            this.richTextBoxDashTasks.Name = "richTextBoxDashTasks";
            this.richTextBoxDashTasks.ReadOnly = true;
            this.richTextBoxDashTasks.Size = new System.Drawing.Size(834, 474);
            this.richTextBoxDashTasks.TabIndex = 0;
            this.richTextBoxDashTasks.Text = "";
            // 
            // tabPageTaskList
            // 
            this.tabPageTaskList.BackColor = System.Drawing.Color.Transparent;
            this.tabPageTaskList.Controls.Add(this.TasksSelection);
            this.tabPageTaskList.Controls.Add(this.labelTodaysTasksFileNotFound);
            this.tabPageTaskList.ImageIndex = 1;
            this.tabPageTaskList.Location = new System.Drawing.Point(4, 54);
            this.tabPageTaskList.Name = "tabPageTaskList";
            this.tabPageTaskList.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTaskList.Size = new System.Drawing.Size(1175, 722);
            this.tabPageTaskList.TabIndex = 0;
            this.tabPageTaskList.Text = "Task Lists";
            // 
            // TasksSelection
            // 
            this.TasksSelection.Controls.Add(this.tabPageChooseTodaysTasks);
            this.TasksSelection.Controls.Add(this.tabPageChooseWeeklyTasks);
            this.TasksSelection.Controls.Add(this.tabPageChooseAddTasks);
            this.TasksSelection.ItemSize = new System.Drawing.Size(79, 50);
            this.TasksSelection.Location = new System.Drawing.Point(7, 6);
            this.TasksSelection.Name = "TasksSelection";
            this.TasksSelection.SelectedIndex = 0;
            this.TasksSelection.Size = new System.Drawing.Size(995, 901);
            this.TasksSelection.TabIndex = 7;
            // 
            // tabPageChooseTodaysTasks
            // 
            this.tabPageChooseTodaysTasks.Controls.Add(this.buttonNoDailyTasks);
            this.tabPageChooseTodaysTasks.Controls.Add(this.labelNoDailyItems);
            this.tabPageChooseTodaysTasks.Controls.Add(this.panel4);
            this.tabPageChooseTodaysTasks.Controls.Add(this.labelTodaysTasksUpdate);
            this.tabPageChooseTodaysTasks.Controls.Add(this.dateTimePicker1);
            this.tabPageChooseTodaysTasks.Controls.Add(this.buttonTodaysTasksUpdate);
            this.tabPageChooseTodaysTasks.Controls.Add(this.labelTodaysDate);
            this.tabPageChooseTodaysTasks.Controls.Add(this.dataGridView1);
            this.tabPageChooseTodaysTasks.Location = new System.Drawing.Point(4, 54);
            this.tabPageChooseTodaysTasks.Name = "tabPageChooseTodaysTasks";
            this.tabPageChooseTodaysTasks.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageChooseTodaysTasks.Size = new System.Drawing.Size(987, 843);
            this.tabPageChooseTodaysTasks.TabIndex = 0;
            this.tabPageChooseTodaysTasks.Text = "Daily Tasks";
            this.tabPageChooseTodaysTasks.UseVisualStyleBackColor = true;
            // 
            // buttonNoDailyTasks
            // 
            this.buttonNoDailyTasks.Location = new System.Drawing.Point(21, 333);
            this.buttonNoDailyTasks.Name = "buttonNoDailyTasks";
            this.buttonNoDailyTasks.Size = new System.Drawing.Size(92, 23);
            this.buttonNoDailyTasks.TabIndex = 42;
            this.buttonNoDailyTasks.Text = "Create Items";
            this.buttonNoDailyTasks.UseVisualStyleBackColor = true;
            this.buttonNoDailyTasks.Click += new System.EventHandler(this.noTasks);
            // 
            // labelNoDailyItems
            // 
            this.labelNoDailyItems.AutoSize = true;
            this.labelNoDailyItems.Location = new System.Drawing.Point(18, 314);
            this.labelNoDailyItems.Name = "labelNoDailyItems";
            this.labelNoDailyItems.Size = new System.Drawing.Size(383, 17);
            this.labelNoDailyItems.TabIndex = 41;
            this.labelNoDailyItems.Text = "There are no items for this day, would you like to add one?";
            // 
            // panel4
            // 
            this.panel4.AutoScroll = true;
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.Controls.Add(this.checkBoxAddOneOffTask);
            this.panel4.Controls.Add(this.labelAddOneOffTaskValidate);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.richTextBoxTodaysTaskAddDescrition);
            this.panel4.Controls.Add(this.buttonTodaysTasksAddExtraTask);
            this.panel4.Controls.Add(this.labelTodaysTasksAddExtraTashHeader);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.labelTodaysTasksExtraTaskName);
            this.panel4.Controls.Add(this.textBoxTodaysTasksExtraTask);
            this.panel4.Location = new System.Drawing.Point(12, 362);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(754, 270);
            this.panel4.TabIndex = 21;
            // 
            // checkBoxAddOneOffTask
            // 
            this.checkBoxAddOneOffTask.AutoSize = true;
            this.checkBoxAddOneOffTask.Location = new System.Drawing.Point(460, 34);
            this.checkBoxAddOneOffTask.Name = "checkBoxAddOneOffTask";
            this.checkBoxAddOneOffTask.Size = new System.Drawing.Size(200, 21);
            this.checkBoxAddOneOffTask.TabIndex = 22;
            this.checkBoxAddOneOffTask.Text = "Has task been completed?";
            this.checkBoxAddOneOffTask.UseVisualStyleBackColor = true;
            // 
            // labelAddOneOffTaskValidate
            // 
            this.labelAddOneOffTaskValidate.AutoSize = true;
            this.labelAddOneOffTaskValidate.Location = new System.Drawing.Point(0, 153);
            this.labelAddOneOffTaskValidate.Name = "labelAddOneOffTaskValidate";
            this.labelAddOneOffTaskValidate.Size = new System.Drawing.Size(0, 17);
            this.labelAddOneOffTaskValidate.TabIndex = 21;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 60);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(86, 17);
            this.label15.TabIndex = 20;
            this.label15.Text = "Description:";
            // 
            // richTextBoxTodaysTaskAddDescrition
            // 
            this.richTextBoxTodaysTaskAddDescrition.Location = new System.Drawing.Point(89, 60);
            this.richTextBoxTodaysTaskAddDescrition.Name = "richTextBoxTodaysTaskAddDescrition";
            this.richTextBoxTodaysTaskAddDescrition.Size = new System.Drawing.Size(553, 135);
            this.richTextBoxTodaysTaskAddDescrition.TabIndex = 19;
            this.richTextBoxTodaysTaskAddDescrition.Text = "";
            // 
            // buttonTodaysTasksAddExtraTask
            // 
            this.buttonTodaysTasksAddExtraTask.BackColor = System.Drawing.Color.Black;
            this.buttonTodaysTasksAddExtraTask.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonTodaysTasksAddExtraTask.Location = new System.Drawing.Point(648, 111);
            this.buttonTodaysTasksAddExtraTask.Name = "buttonTodaysTasksAddExtraTask";
            this.buttonTodaysTasksAddExtraTask.Size = new System.Drawing.Size(75, 25);
            this.buttonTodaysTasksAddExtraTask.TabIndex = 18;
            this.buttonTodaysTasksAddExtraTask.Text = "Enter Task";
            this.buttonTodaysTasksAddExtraTask.UseVisualStyleBackColor = false;
            this.buttonTodaysTasksAddExtraTask.Click += new System.EventHandler(this.updateAddOneOffDailyTask);
            // 
            // labelTodaysTasksAddExtraTashHeader
            // 
            this.labelTodaysTasksAddExtraTashHeader.AutoSize = true;
            this.labelTodaysTasksAddExtraTashHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTodaysTasksAddExtraTashHeader.Location = new System.Drawing.Point(3, 3);
            this.labelTodaysTasksAddExtraTashHeader.Name = "labelTodaysTasksAddExtraTashHeader";
            this.labelTodaysTasksAddExtraTashHeader.Size = new System.Drawing.Size(172, 13);
            this.labelTodaysTasksAddExtraTashHeader.TabIndex = 12;
            this.labelTodaysTasksAddExtraTashHeader.Text = "Add an extra task for the day";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 88);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 17);
            this.label16.TabIndex = 9;
            // 
            // labelTodaysTasksExtraTaskName
            // 
            this.labelTodaysTasksExtraTaskName.AutoSize = true;
            this.labelTodaysTasksExtraTaskName.Location = new System.Drawing.Point(6, 30);
            this.labelTodaysTasksExtraTaskName.Name = "labelTodaysTasksExtraTaskName";
            this.labelTodaysTasksExtraTaskName.Size = new System.Drawing.Size(85, 17);
            this.labelTodaysTasksExtraTaskName.TabIndex = 17;
            this.labelTodaysTasksExtraTaskName.Text = "Task Name:";
            // 
            // textBoxTodaysTasksExtraTask
            // 
            this.textBoxTodaysTasksExtraTask.Location = new System.Drawing.Point(89, 32);
            this.textBoxTodaysTasksExtraTask.Name = "textBoxTodaysTasksExtraTask";
            this.textBoxTodaysTasksExtraTask.Size = new System.Drawing.Size(365, 25);
            this.textBoxTodaysTasksExtraTask.TabIndex = 16;
            // 
            // labelTodaysTasksUpdate
            // 
            this.labelTodaysTasksUpdate.AutoSize = true;
            this.labelTodaysTasksUpdate.Location = new System.Drawing.Point(480, 427);
            this.labelTodaysTasksUpdate.Name = "labelTodaysTasksUpdate";
            this.labelTodaysTasksUpdate.Size = new System.Drawing.Size(0, 17);
            this.labelTodaysTasksUpdate.TabIndex = 6;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(7, 7);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(236, 25);
            this.dateTimePicker1.TabIndex = 10;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.loadSelectedDailyTask);
            // 
            // buttonTodaysTasksUpdate
            // 
            this.buttonTodaysTasksUpdate.BackColor = System.Drawing.Color.Black;
            this.buttonTodaysTasksUpdate.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonTodaysTasksUpdate.Location = new System.Drawing.Point(628, 331);
            this.buttonTodaysTasksUpdate.Name = "buttonTodaysTasksUpdate";
            this.buttonTodaysTasksUpdate.Size = new System.Drawing.Size(138, 32);
            this.buttonTodaysTasksUpdate.TabIndex = 8;
            this.buttonTodaysTasksUpdate.Text = "Save Todays Tasks";
            this.buttonTodaysTasksUpdate.UseVisualStyleBackColor = false;
            this.buttonTodaysTasksUpdate.Click += new System.EventHandler(this.updateTodaysTasks);
            // 
            // labelTodaysDate
            // 
            this.labelTodaysDate.AutoSize = true;
            this.labelTodaysDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTodaysDate.Location = new System.Drawing.Point(21, 34);
            this.labelTodaysDate.Name = "labelTodaysDate";
            this.labelTodaysDate.Size = new System.Drawing.Size(159, 16);
            this.labelTodaysDate.TabIndex = 7;
            this.labelTodaysDate.Text = "Todays task checklist";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowDrop = true;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.task,
            this.TaskDescription,
            this.completed,
            this.Date});
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView1.Location = new System.Drawing.Point(12, 53);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(754, 310);
            this.dataGridView1.TabIndex = 6;
            // 
            // task
            // 
            this.task.DataPropertyName = "task";
            this.task.HeaderText = "Task";
            this.task.Name = "task";
            this.task.ReadOnly = true;
            this.task.Width = 200;
            // 
            // TaskDescription
            // 
            this.TaskDescription.DataPropertyName = "description";
            this.TaskDescription.HeaderText = "Description";
            this.TaskDescription.Name = "TaskDescription";
            // 
            // completed
            // 
            this.completed.DataPropertyName = "completed";
            this.completed.HeaderText = "Completed";
            this.completed.Name = "completed";
            // 
            // Date
            // 
            this.Date.DataPropertyName = "date";
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // tabPageChooseWeeklyTasks
            // 
            this.tabPageChooseWeeklyTasks.Controls.Add(this.buttonExtraTasksUpdate);
            this.tabPageChooseWeeklyTasks.Controls.Add(this.labelExtraTasksDate);
            this.tabPageChooseWeeklyTasks.Controls.Add(this.labelExtraTasksHeader);
            this.tabPageChooseWeeklyTasks.Controls.Add(this.dataGridView3);
            this.tabPageChooseWeeklyTasks.Location = new System.Drawing.Point(4, 54);
            this.tabPageChooseWeeklyTasks.Name = "tabPageChooseWeeklyTasks";
            this.tabPageChooseWeeklyTasks.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageChooseWeeklyTasks.Size = new System.Drawing.Size(987, 843);
            this.tabPageChooseWeeklyTasks.TabIndex = 1;
            this.tabPageChooseWeeklyTasks.Text = "Extra Tasks";
            this.tabPageChooseWeeklyTasks.UseVisualStyleBackColor = true;
            // 
            // buttonExtraTasksUpdate
            // 
            this.buttonExtraTasksUpdate.BackColor = System.Drawing.Color.Black;
            this.buttonExtraTasksUpdate.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonExtraTasksUpdate.Location = new System.Drawing.Point(622, 478);
            this.buttonExtraTasksUpdate.Name = "buttonExtraTasksUpdate";
            this.buttonExtraTasksUpdate.Size = new System.Drawing.Size(138, 32);
            this.buttonExtraTasksUpdate.TabIndex = 13;
            this.buttonExtraTasksUpdate.Text = "Save Extra Tasks";
            this.buttonExtraTasksUpdate.UseVisualStyleBackColor = false;
            // 
            // labelExtraTasksDate
            // 
            this.labelExtraTasksDate.AutoSize = true;
            this.labelExtraTasksDate.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExtraTasksDate.Location = new System.Drawing.Point(193, 48);
            this.labelExtraTasksDate.Name = "labelExtraTasksDate";
            this.labelExtraTasksDate.Size = new System.Drawing.Size(0, 18);
            this.labelExtraTasksDate.TabIndex = 11;
            // 
            // labelExtraTasksHeader
            // 
            this.labelExtraTasksHeader.AutoSize = true;
            this.labelExtraTasksHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExtraTasksHeader.Location = new System.Drawing.Point(21, 18);
            this.labelExtraTasksHeader.Name = "labelExtraTasksHeader";
            this.labelExtraTasksHeader.Size = new System.Drawing.Size(158, 16);
            this.labelExtraTasksHeader.TabIndex = 10;
            this.labelExtraTasksHeader.Text = "Weekly task checklist";
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.weekly_or_monthly,
            this.dataGridViewCheckBoxColumn1,
            this.date_completed});
            this.dataGridView3.Location = new System.Drawing.Point(15, 48);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.Size = new System.Drawing.Size(745, 424);
            this.dataGridView3.TabIndex = 9;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "task";
            this.dataGridViewTextBoxColumn1.HeaderText = "Task";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 200;
            // 
            // weekly_or_monthly
            // 
            this.weekly_or_monthly.DataPropertyName = "weekly_or_monthly";
            this.weekly_or_monthly.HeaderText = "Frequency";
            this.weekly_or_monthly.Name = "weekly_or_monthly";
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "completed";
            this.dataGridViewCheckBoxColumn1.HeaderText = "Completed";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            // 
            // date_completed
            // 
            this.date_completed.DataPropertyName = "date_completed";
            this.date_completed.HeaderText = "date_completed";
            this.date_completed.Name = "date_completed";
            this.date_completed.Visible = false;
            // 
            // tabPageChooseAddTasks
            // 
            this.tabPageChooseAddTasks.Controls.Add(this.buttonAddTasksUpdate);
            this.tabPageChooseAddTasks.Controls.Add(this.panel2);
            this.tabPageChooseAddTasks.Controls.Add(this.labelFutureTasksHeader);
            this.tabPageChooseAddTasks.Controls.Add(this.label1);
            this.tabPageChooseAddTasks.Controls.Add(this.dataGridView2);
            this.tabPageChooseAddTasks.Location = new System.Drawing.Point(4, 54);
            this.tabPageChooseAddTasks.Name = "tabPageChooseAddTasks";
            this.tabPageChooseAddTasks.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageChooseAddTasks.Size = new System.Drawing.Size(987, 843);
            this.tabPageChooseAddTasks.TabIndex = 2;
            this.tabPageChooseAddTasks.Text = "Manage Tasks";
            this.tabPageChooseAddTasks.UseVisualStyleBackColor = true;
            // 
            // buttonAddTasksUpdate
            // 
            this.buttonAddTasksUpdate.BackColor = System.Drawing.Color.Black;
            this.buttonAddTasksUpdate.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonAddTasksUpdate.Location = new System.Drawing.Point(649, 363);
            this.buttonAddTasksUpdate.Name = "buttonAddTasksUpdate";
            this.buttonAddTasksUpdate.Size = new System.Drawing.Size(114, 34);
            this.buttonAddTasksUpdate.TabIndex = 21;
            this.buttonAddTasksUpdate.Text = "Save All Tasks";
            this.buttonAddTasksUpdate.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.radioButtonSixMonthly);
            this.panel2.Controls.Add(this.radioButtonThreeMonthly);
            this.panel2.Controls.Add(this.radioButtonTwoMonthly);
            this.panel2.Controls.Add(this.buttonFutureTaskAdd);
            this.panel2.Controls.Add(this.labelFutureTasksHeader1);
            this.panel2.Controls.Add(this.labelAddTaskValidate);
            this.panel2.Controls.Add(this.labelFutureTaskName);
            this.panel2.Controls.Add(this.radioButtonDailyTask);
            this.panel2.Controls.Add(this.textBoxFutureName);
            this.panel2.Controls.Add(this.radioButtonWeeklyTask);
            this.panel2.Controls.Add(this.radioButtonMonthlyTask);
            this.panel2.Location = new System.Drawing.Point(12, 403);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(751, 182);
            this.panel2.TabIndex = 20;
            // 
            // radioButtonSixMonthly
            // 
            this.radioButtonSixMonthly.AutoSize = true;
            this.radioButtonSixMonthly.Location = new System.Drawing.Point(365, 118);
            this.radioButtonSixMonthly.Name = "radioButtonSixMonthly";
            this.radioButtonSixMonthly.Size = new System.Drawing.Size(133, 21);
            this.radioButtonSixMonthly.TabIndex = 21;
            this.radioButtonSixMonthly.Text = "Six Monthly Task";
            this.radioButtonSixMonthly.UseVisualStyleBackColor = true;
            // 
            // radioButtonThreeMonthly
            // 
            this.radioButtonThreeMonthly.AutoSize = true;
            this.radioButtonThreeMonthly.Location = new System.Drawing.Point(227, 118);
            this.radioButtonThreeMonthly.Name = "radioButtonThreeMonthly";
            this.radioButtonThreeMonthly.Size = new System.Drawing.Size(151, 21);
            this.radioButtonThreeMonthly.TabIndex = 20;
            this.radioButtonThreeMonthly.Text = "Three Monthly Task";
            this.radioButtonThreeMonthly.UseVisualStyleBackColor = true;
            // 
            // radioButtonTwoMonthly
            // 
            this.radioButtonTwoMonthly.AutoSize = true;
            this.radioButtonTwoMonthly.Location = new System.Drawing.Point(92, 118);
            this.radioButtonTwoMonthly.Name = "radioButtonTwoMonthly";
            this.radioButtonTwoMonthly.Size = new System.Drawing.Size(140, 21);
            this.radioButtonTwoMonthly.TabIndex = 19;
            this.radioButtonTwoMonthly.Text = "Two Monthly Task";
            this.radioButtonTwoMonthly.UseVisualStyleBackColor = true;
            // 
            // buttonFutureTaskAdd
            // 
            this.buttonFutureTaskAdd.BackColor = System.Drawing.Color.Black;
            this.buttonFutureTaskAdd.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonFutureTaskAdd.Location = new System.Drawing.Point(381, 63);
            this.buttonFutureTaskAdd.Name = "buttonFutureTaskAdd";
            this.buttonFutureTaskAdd.Size = new System.Drawing.Size(75, 25);
            this.buttonFutureTaskAdd.TabIndex = 18;
            this.buttonFutureTaskAdd.Text = "Enter Task";
            this.buttonFutureTaskAdd.UseVisualStyleBackColor = false;
            this.buttonFutureTaskAdd.Click += new System.EventHandler(this.addTask);
            // 
            // labelFutureTasksHeader1
            // 
            this.labelFutureTasksHeader1.AutoSize = true;
            this.labelFutureTasksHeader1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFutureTasksHeader1.Location = new System.Drawing.Point(7, 16);
            this.labelFutureTasksHeader1.Name = "labelFutureTasksHeader1";
            this.labelFutureTasksHeader1.Size = new System.Drawing.Size(283, 13);
            this.labelFutureTasksHeader1.TabIndex = 12;
            this.labelFutureTasksHeader1.Text = "Add a task and how often it has to be completed";
            // 
            // labelAddTaskValidate
            // 
            this.labelAddTaskValidate.AutoSize = true;
            this.labelAddTaskValidate.Location = new System.Drawing.Point(10, 122);
            this.labelAddTaskValidate.Name = "labelAddTaskValidate";
            this.labelAddTaskValidate.Size = new System.Drawing.Size(0, 17);
            this.labelAddTaskValidate.TabIndex = 9;
            // 
            // labelFutureTaskName
            // 
            this.labelFutureTaskName.AutoSize = true;
            this.labelFutureTaskName.Location = new System.Drawing.Point(10, 43);
            this.labelFutureTaskName.Name = "labelFutureTaskName";
            this.labelFutureTaskName.Size = new System.Drawing.Size(85, 17);
            this.labelFutureTaskName.TabIndex = 17;
            this.labelFutureTaskName.Text = "Task Name:";
            // 
            // radioButtonDailyTask
            // 
            this.radioButtonDailyTask.AutoSize = true;
            this.radioButtonDailyTask.Checked = true;
            this.radioButtonDailyTask.Location = new System.Drawing.Point(92, 94);
            this.radioButtonDailyTask.Name = "radioButtonDailyTask";
            this.radioButtonDailyTask.Size = new System.Drawing.Size(92, 21);
            this.radioButtonDailyTask.TabIndex = 13;
            this.radioButtonDailyTask.TabStop = true;
            this.radioButtonDailyTask.Text = "Daily Task";
            this.radioButtonDailyTask.UseVisualStyleBackColor = true;
            // 
            // textBoxFutureName
            // 
            this.textBoxFutureName.Location = new System.Drawing.Point(92, 42);
            this.textBoxFutureName.Multiline = true;
            this.textBoxFutureName.Name = "textBoxFutureName";
            this.textBoxFutureName.Size = new System.Drawing.Size(283, 46);
            this.textBoxFutureName.TabIndex = 16;
            // 
            // radioButtonWeeklyTask
            // 
            this.radioButtonWeeklyTask.AutoSize = true;
            this.radioButtonWeeklyTask.Location = new System.Drawing.Point(184, 94);
            this.radioButtonWeeklyTask.Name = "radioButtonWeeklyTask";
            this.radioButtonWeeklyTask.Size = new System.Drawing.Size(108, 21);
            this.radioButtonWeeklyTask.TabIndex = 14;
            this.radioButtonWeeklyTask.Text = "Weekly Task";
            this.radioButtonWeeklyTask.UseVisualStyleBackColor = true;
            // 
            // radioButtonMonthlyTask
            // 
            this.radioButtonMonthlyTask.AutoSize = true;
            this.radioButtonMonthlyTask.Location = new System.Drawing.Point(291, 94);
            this.radioButtonMonthlyTask.Name = "radioButtonMonthlyTask";
            this.radioButtonMonthlyTask.Size = new System.Drawing.Size(109, 21);
            this.radioButtonMonthlyTask.TabIndex = 15;
            this.radioButtonMonthlyTask.Text = "Monthly Task";
            this.radioButtonMonthlyTask.UseVisualStyleBackColor = true;
            // 
            // labelFutureTasksHeader
            // 
            this.labelFutureTasksHeader.AutoSize = true;
            this.labelFutureTasksHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFutureTasksHeader.Location = new System.Drawing.Point(6, 16);
            this.labelFutureTasksHeader.Name = "labelFutureTasksHeader";
            this.labelFutureTasksHeader.Size = new System.Drawing.Size(338, 16);
            this.labelFutureTasksHeader.TabIndex = 13;
            this.labelFutureTasksHeader.Text = "Add a task and how often it has to be completed";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(494, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "A list of all the tasks you have added so far and how often they have to be compl" +
                "eted";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.task_name,
            this.frequency});
            this.dataGridView2.Location = new System.Drawing.Point(6, 69);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(757, 350);
            this.dataGridView2.TabIndex = 11;
            // 
            // task_name
            // 
            this.task_name.DataPropertyName = "task_name";
            this.task_name.HeaderText = "Task Name";
            this.task_name.Name = "task_name";
            this.task_name.Width = 200;
            // 
            // frequency
            // 
            this.frequency.DataPropertyName = "frequency";
            this.frequency.HeaderText = "Frequency";
            this.frequency.Name = "frequency";
            // 
            // labelTodaysTasksFileNotFound
            // 
            this.labelTodaysTasksFileNotFound.AutoSize = true;
            this.labelTodaysTasksFileNotFound.Location = new System.Drawing.Point(6, 408);
            this.labelTodaysTasksFileNotFound.Name = "labelTodaysTasksFileNotFound";
            this.labelTodaysTasksFileNotFound.Size = new System.Drawing.Size(0, 17);
            this.labelTodaysTasksFileNotFound.TabIndex = 3;
            // 
            // tabPageInventoryMaintenence
            // 
            this.tabPageInventoryMaintenence.Controls.Add(this.tabControl3);
            this.tabPageInventoryMaintenence.Controls.Add(this.labelExtraTasksUpdate);
            this.tabPageInventoryMaintenence.ImageIndex = 2;
            this.tabPageInventoryMaintenence.Location = new System.Drawing.Point(4, 54);
            this.tabPageInventoryMaintenence.Name = "tabPageInventoryMaintenence";
            this.tabPageInventoryMaintenence.Size = new System.Drawing.Size(1175, 722);
            this.tabPageInventoryMaintenence.TabIndex = 4;
            this.tabPageInventoryMaintenence.Text = "Maintenence";
            this.tabPageInventoryMaintenence.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPageItemMaintenence);
            this.tabControl3.Controls.Add(this.tabPageInventory);
            this.tabControl3.ItemSize = new System.Drawing.Size(115, 50);
            this.tabControl3.Location = new System.Drawing.Point(4, 5);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(995, 900);
            this.tabControl3.TabIndex = 11;
            // 
            // tabPageItemMaintenence
            // 
            this.tabPageItemMaintenence.Controls.Add(this.buttonNoMaintenenceItems);
            this.tabPageItemMaintenence.Controls.Add(this.labelMaintenenceNoTasks);
            this.tabPageItemMaintenence.Controls.Add(this.dateTimePicker3);
            this.tabPageItemMaintenence.Controls.Add(this.button3);
            this.tabPageItemMaintenence.Controls.Add(this.panel5);
            this.tabPageItemMaintenence.Controls.Add(this.dataGridView6);
            this.tabPageItemMaintenence.Location = new System.Drawing.Point(4, 54);
            this.tabPageItemMaintenence.Name = "tabPageItemMaintenence";
            this.tabPageItemMaintenence.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageItemMaintenence.Size = new System.Drawing.Size(987, 842);
            this.tabPageItemMaintenence.TabIndex = 0;
            this.tabPageItemMaintenence.Text = "Item Maintenence";
            this.tabPageItemMaintenence.UseVisualStyleBackColor = true;
            // 
            // buttonNoMaintenenceItems
            // 
            this.buttonNoMaintenenceItems.Location = new System.Drawing.Point(36, 266);
            this.buttonNoMaintenenceItems.Name = "buttonNoMaintenenceItems";
            this.buttonNoMaintenenceItems.Size = new System.Drawing.Size(92, 23);
            this.buttonNoMaintenenceItems.TabIndex = 40;
            this.buttonNoMaintenenceItems.Text = "Create Items";
            this.buttonNoMaintenenceItems.UseVisualStyleBackColor = true;
            this.buttonNoMaintenenceItems.Click += new System.EventHandler(this.noMaintenence);
            // 
            // labelMaintenenceNoTasks
            // 
            this.labelMaintenenceNoTasks.AutoSize = true;
            this.labelMaintenenceNoTasks.Location = new System.Drawing.Point(33, 247);
            this.labelMaintenenceNoTasks.Name = "labelMaintenenceNoTasks";
            this.labelMaintenenceNoTasks.Size = new System.Drawing.Size(383, 17);
            this.labelMaintenenceNoTasks.TabIndex = 39;
            this.labelMaintenenceNoTasks.Text = "There are no items for this day, would you like to add one?";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Location = new System.Drawing.Point(7, 7);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(235, 25);
            this.dateTimePicker3.TabIndex = 38;
            this.dateTimePicker3.ValueChanged += new System.EventHandler(this.loadSelectedMaintenenceItems);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Black;
            this.button3.ForeColor = System.Drawing.SystemColors.Window;
            this.button3.Location = new System.Drawing.Point(619, 320);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(168, 32);
            this.button3.TabIndex = 37;
            this.button3.Text = "Update Item Maintenence";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.updateMaintenenceItems);
            // 
            // panel5
            // 
            this.panel5.AutoScroll = true;
            this.panel5.AutoSize = true;
            this.panel5.Controls.Add(this.buttonAddItemMaintenence);
            this.panel5.Controls.Add(this.richTextBoxAddItemMaintenceDescription);
            this.panel5.Controls.Add(this.textBoxItemMaintenenceArea);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.textBoxAddItemMaintenenceItem);
            this.panel5.Controls.Add(this.textBoxAddItemMaintenceRoom);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Location = new System.Drawing.Point(22, 358);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(765, 523);
            this.panel5.TabIndex = 36;
            // 
            // buttonAddItemMaintenence
            // 
            this.buttonAddItemMaintenence.BackColor = System.Drawing.Color.Black;
            this.buttonAddItemMaintenence.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonAddItemMaintenence.Location = new System.Drawing.Point(395, 251);
            this.buttonAddItemMaintenence.Name = "buttonAddItemMaintenence";
            this.buttonAddItemMaintenence.Size = new System.Drawing.Size(168, 32);
            this.buttonAddItemMaintenence.TabIndex = 41;
            this.buttonAddItemMaintenence.Text = "Add Item Maintenence";
            this.buttonAddItemMaintenence.UseVisualStyleBackColor = false;
            // 
            // richTextBoxAddItemMaintenceDescription
            // 
            this.richTextBoxAddItemMaintenceDescription.Location = new System.Drawing.Point(14, 36);
            this.richTextBoxAddItemMaintenceDescription.Name = "richTextBoxAddItemMaintenceDescription";
            this.richTextBoxAddItemMaintenceDescription.Size = new System.Drawing.Size(560, 126);
            this.richTextBoxAddItemMaintenceDescription.TabIndex = 35;
            this.richTextBoxAddItemMaintenceDescription.Text = "";
            // 
            // textBoxItemMaintenenceArea
            // 
            this.textBoxItemMaintenenceArea.Location = new System.Drawing.Point(56, 282);
            this.textBoxItemMaintenenceArea.Multiline = true;
            this.textBoxItemMaintenenceArea.Name = "textBoxItemMaintenenceArea";
            this.textBoxItemMaintenenceArea.Size = new System.Drawing.Size(239, 38);
            this.textBoxItemMaintenenceArea.TabIndex = 40;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(11, 14);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(192, 17);
            this.label18.TabIndex = 34;
            this.label18.Text = "Description Of Maintenence:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(10, 285);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 17);
            this.label22.TabIndex = 39;
            this.label22.Text = "Area:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(233, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(318, 17);
            this.label20.TabIndex = 35;
            this.label20.Text = "Select a item and add details of its maintenence";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(13, 177);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 17);
            this.label19.TabIndex = 32;
            this.label19.Text = "Item:";
            // 
            // textBoxAddItemMaintenenceItem
            // 
            this.textBoxAddItemMaintenenceItem.Location = new System.Drawing.Point(56, 177);
            this.textBoxAddItemMaintenenceItem.Multiline = true;
            this.textBoxAddItemMaintenenceItem.Name = "textBoxAddItemMaintenenceItem";
            this.textBoxAddItemMaintenenceItem.Size = new System.Drawing.Size(239, 34);
            this.textBoxAddItemMaintenenceItem.TabIndex = 33;
            // 
            // textBoxAddItemMaintenceRoom
            // 
            this.textBoxAddItemMaintenceRoom.Location = new System.Drawing.Point(56, 232);
            this.textBoxAddItemMaintenceRoom.Multiline = true;
            this.textBoxAddItemMaintenceRoom.Name = "textBoxAddItemMaintenceRoom";
            this.textBoxAddItemMaintenceRoom.Size = new System.Drawing.Size(239, 34);
            this.textBoxAddItemMaintenceRoom.TabIndex = 38;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 232);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 17);
            this.label21.TabIndex = 37;
            this.label21.Text = "Room:";
            // 
            // dataGridView6
            // 
            this.dataGridView6.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Item,
            this.dataGridViewTextBoxColumn5,
            this.Room,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.dataGridView6.Location = new System.Drawing.Point(22, 47);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.Size = new System.Drawing.Size(765, 305);
            this.dataGridView6.TabIndex = 34;
            // 
            // Item
            // 
            this.Item.DataPropertyName = "item_name";
            this.Item.HeaderText = "Item";
            this.Item.Name = "Item";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "area";
            this.dataGridViewTextBoxColumn5.HeaderText = "Area";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // Room
            // 
            this.Room.DataPropertyName = "room";
            this.Room.HeaderText = "Room";
            this.Room.Name = "Room";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "description";
            this.dataGridViewTextBoxColumn7.HeaderText = "Description";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "date";
            this.dataGridViewTextBoxColumn8.HeaderText = "Date";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // tabPageInventory
            // 
            this.tabPageInventory.Controls.Add(this.panel6);
            this.tabPageInventory.Controls.Add(this.dataGridView7);
            this.tabPageInventory.Location = new System.Drawing.Point(4, 54);
            this.tabPageInventory.Name = "tabPageInventory";
            this.tabPageInventory.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageInventory.Size = new System.Drawing.Size(987, 842);
            this.tabPageInventory.TabIndex = 1;
            this.tabPageInventory.Text = "Inventory Stock List";
            this.tabPageInventory.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.AutoScroll = true;
            this.panel6.Controls.Add(this.buttonUpdateInventory);
            this.panel6.Controls.Add(this.labelAddInventoryValidate);
            this.panel6.Controls.Add(this.textBoxAddInventoryArea);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.textBoxAddInventoryQuantity);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.buttonAddInventroyItem);
            this.panel6.Controls.Add(this.textBoxAddInventoryItem);
            this.panel6.Controls.Add(this.label26);
            this.panel6.Location = new System.Drawing.Point(24, 344);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(723, 219);
            this.panel6.TabIndex = 40;
            // 
            // buttonUpdateInventory
            // 
            this.buttonUpdateInventory.BackColor = System.Drawing.Color.Black;
            this.buttonUpdateInventory.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonUpdateInventory.Location = new System.Drawing.Point(535, 0);
            this.buttonUpdateInventory.Name = "buttonUpdateInventory";
            this.buttonUpdateInventory.Size = new System.Drawing.Size(168, 32);
            this.buttonUpdateInventory.TabIndex = 41;
            this.buttonUpdateInventory.Text = "Update Item Inventory";
            this.buttonUpdateInventory.UseVisualStyleBackColor = false;
            this.buttonUpdateInventory.Click += new System.EventHandler(this.updateInventory);
            // 
            // labelAddInventoryValidate
            // 
            this.labelAddInventoryValidate.AutoSize = true;
            this.labelAddInventoryValidate.Location = new System.Drawing.Point(17, 136);
            this.labelAddInventoryValidate.Name = "labelAddInventoryValidate";
            this.labelAddInventoryValidate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelAddInventoryValidate.Size = new System.Drawing.Size(0, 17);
            this.labelAddInventoryValidate.TabIndex = 41;
            // 
            // textBoxAddInventoryArea
            // 
            this.textBoxAddInventoryArea.Location = new System.Drawing.Point(88, 136);
            this.textBoxAddInventoryArea.Multiline = true;
            this.textBoxAddInventoryArea.Name = "textBoxAddInventoryArea";
            this.textBoxAddInventoryArea.Size = new System.Drawing.Size(223, 39);
            this.textBoxAddInventoryArea.TabIndex = 40;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(14, 136);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 17);
            this.label23.TabIndex = 39;
            this.label23.Text = "Area:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(3, 14);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(285, 17);
            this.label27.TabIndex = 39;
            this.label27.Text = "Select an item to keep track of its inventory";
            // 
            // textBoxAddInventoryQuantity
            // 
            this.textBoxAddInventoryQuantity.Location = new System.Drawing.Point(88, 85);
            this.textBoxAddInventoryQuantity.Multiline = true;
            this.textBoxAddInventoryQuantity.Name = "textBoxAddInventoryQuantity";
            this.textBoxAddInventoryQuantity.Size = new System.Drawing.Size(223, 42);
            this.textBoxAddInventoryQuantity.TabIndex = 38;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(14, 95);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(66, 17);
            this.label24.TabIndex = 37;
            this.label24.Text = "Quantity:";
            // 
            // buttonAddInventroyItem
            // 
            this.buttonAddInventroyItem.BackColor = System.Drawing.Color.Black;
            this.buttonAddInventroyItem.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonAddInventroyItem.Location = new System.Drawing.Point(317, 143);
            this.buttonAddInventroyItem.Name = "buttonAddInventroyItem";
            this.buttonAddInventroyItem.Size = new System.Drawing.Size(114, 32);
            this.buttonAddInventroyItem.TabIndex = 36;
            this.buttonAddInventroyItem.Text = "Add Item";
            this.buttonAddInventroyItem.UseVisualStyleBackColor = false;
            this.buttonAddInventroyItem.Click += new System.EventHandler(this.addItemMaintenence);
            // 
            // textBoxAddInventoryItem
            // 
            this.textBoxAddInventoryItem.Location = new System.Drawing.Point(88, 44);
            this.textBoxAddInventoryItem.Multiline = true;
            this.textBoxAddInventoryItem.Name = "textBoxAddInventoryItem";
            this.textBoxAddInventoryItem.Size = new System.Drawing.Size(223, 35);
            this.textBoxAddInventoryItem.TabIndex = 33;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(14, 44);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(40, 17);
            this.label26.TabIndex = 32;
            this.label26.Text = "Item:";
            // 
            // dataGridView7
            // 
            this.dataGridView7.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView7.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10});
            this.dataGridView7.Location = new System.Drawing.Point(24, 44);
            this.dataGridView7.Name = "dataGridView7";
            this.dataGridView7.Size = new System.Drawing.Size(723, 304);
            this.dataGridView7.TabIndex = 38;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "area";
            this.dataGridViewTextBoxColumn6.HeaderText = "Area";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "quantity";
            this.dataGridViewTextBoxColumn9.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "item";
            this.dataGridViewTextBoxColumn10.HeaderText = "Item";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // labelExtraTasksUpdate
            // 
            this.labelExtraTasksUpdate.AutoSize = true;
            this.labelExtraTasksUpdate.Location = new System.Drawing.Point(52, 450);
            this.labelExtraTasksUpdate.Name = "labelExtraTasksUpdate";
            this.labelExtraTasksUpdate.Size = new System.Drawing.Size(0, 17);
            this.labelExtraTasksUpdate.TabIndex = 10;
            // 
            // tabPageMaintenanceLegislation
            // 
            this.tabPageMaintenanceLegislation.Controls.Add(this.labelAddProviderValidate);
            this.tabPageMaintenanceLegislation.Controls.Add(this.label35);
            this.tabPageMaintenanceLegislation.Controls.Add(this.buttonAddServiceProvider);
            this.tabPageMaintenanceLegislation.Controls.Add(this.textBoxAddProviderDateCompleted);
            this.tabPageMaintenanceLegislation.Controls.Add(this.textBoxAddProviderDueDate);
            this.tabPageMaintenanceLegislation.Controls.Add(this.textBoxAddProvidersName);
            this.tabPageMaintenanceLegislation.Controls.Add(this.label34);
            this.tabPageMaintenanceLegislation.Controls.Add(this.label32);
            this.tabPageMaintenanceLegislation.Controls.Add(this.label31);
            this.tabPageMaintenanceLegislation.Controls.Add(this.dataGridView8);
            this.tabPageMaintenanceLegislation.Controls.Add(this.buttonUpdateServiceProviders);
            this.tabPageMaintenanceLegislation.ImageIndex = 3;
            this.tabPageMaintenanceLegislation.Location = new System.Drawing.Point(4, 54);
            this.tabPageMaintenanceLegislation.Name = "tabPageMaintenanceLegislation";
            this.tabPageMaintenanceLegislation.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMaintenanceLegislation.Size = new System.Drawing.Size(1175, 722);
            this.tabPageMaintenanceLegislation.TabIndex = 5;
            this.tabPageMaintenanceLegislation.Text = "Maint Legislation";
            this.tabPageMaintenanceLegislation.UseVisualStyleBackColor = true;
            // 
            // labelAddProviderValidate
            // 
            this.labelAddProviderValidate.AutoSize = true;
            this.labelAddProviderValidate.Location = new System.Drawing.Point(33, 563);
            this.labelAddProviderValidate.Name = "labelAddProviderValidate";
            this.labelAddProviderValidate.Size = new System.Drawing.Size(0, 17);
            this.labelAddProviderValidate.TabIndex = 55;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(20, 503);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(177, 17);
            this.label35.TabIndex = 54;
            this.label35.Text = "Service provider due date:";
            // 
            // buttonAddServiceProvider
            // 
            this.buttonAddServiceProvider.BackColor = System.Drawing.Color.Black;
            this.buttonAddServiceProvider.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonAddServiceProvider.Location = new System.Drawing.Point(418, 516);
            this.buttonAddServiceProvider.Name = "buttonAddServiceProvider";
            this.buttonAddServiceProvider.Size = new System.Drawing.Size(98, 32);
            this.buttonAddServiceProvider.TabIndex = 53;
            this.buttonAddServiceProvider.Text = "Add Provider";
            this.buttonAddServiceProvider.UseVisualStyleBackColor = false;
            this.buttonAddServiceProvider.Click += new System.EventHandler(this.AddProvider);
            // 
            // textBoxAddProviderDateCompleted
            // 
            this.textBoxAddProviderDateCompleted.Location = new System.Drawing.Point(201, 526);
            this.textBoxAddProviderDateCompleted.Name = "textBoxAddProviderDateCompleted";
            this.textBoxAddProviderDateCompleted.Size = new System.Drawing.Size(211, 25);
            this.textBoxAddProviderDateCompleted.TabIndex = 52;
            // 
            // textBoxAddProviderDueDate
            // 
            this.textBoxAddProviderDueDate.Location = new System.Drawing.Point(201, 495);
            this.textBoxAddProviderDueDate.Name = "textBoxAddProviderDueDate";
            this.textBoxAddProviderDueDate.Size = new System.Drawing.Size(211, 25);
            this.textBoxAddProviderDueDate.TabIndex = 51;
            // 
            // textBoxAddProvidersName
            // 
            this.textBoxAddProvidersName.Location = new System.Drawing.Point(201, 466);
            this.textBoxAddProvidersName.Name = "textBoxAddProvidersName";
            this.textBoxAddProvidersName.Size = new System.Drawing.Size(211, 25);
            this.textBoxAddProvidersName.TabIndex = 50;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(39, 529);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(175, 17);
            this.label34.TabIndex = 49;
            this.label34.Text = "Date completed(optional):";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(39, 469);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(158, 17);
            this.label32.TabIndex = 48;
            this.label32.Text = "Service provider name:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(39, 427);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(152, 17);
            this.label31.TabIndex = 47;
            this.label31.Text = "Add a service provider";
            // 
            // dataGridView8
            // 
            this.dataGridView8.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView8.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn11,
            this.date_completed1,
            this.date_due1});
            this.dataGridView8.Location = new System.Drawing.Point(23, 27);
            this.dataGridView8.Name = "dataGridView8";
            this.dataGridView8.Size = new System.Drawing.Size(697, 379);
            this.dataGridView8.TabIndex = 40;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn11.HeaderText = "Service Provider";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // date_completed1
            // 
            this.date_completed1.DataPropertyName = "date_completed";
            this.date_completed1.HeaderText = "Date Completed";
            this.date_completed1.Name = "date_completed1";
            // 
            // date_due1
            // 
            this.date_due1.DataPropertyName = "due_date";
            this.date_due1.HeaderText = "Date Due";
            this.date_due1.Name = "date_due1";
            // 
            // buttonUpdateServiceProviders
            // 
            this.buttonUpdateServiceProviders.BackColor = System.Drawing.Color.Black;
            this.buttonUpdateServiceProviders.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonUpdateServiceProviders.Location = new System.Drawing.Point(588, 412);
            this.buttonUpdateServiceProviders.Name = "buttonUpdateServiceProviders";
            this.buttonUpdateServiceProviders.Size = new System.Drawing.Size(138, 32);
            this.buttonUpdateServiceProviders.TabIndex = 45;
            this.buttonUpdateServiceProviders.Text = "Save Providers";
            this.buttonUpdateServiceProviders.UseVisualStyleBackColor = false;
            this.buttonUpdateServiceProviders.Click += new System.EventHandler(this.updateProviders);
            // 
            // tabPageContractors
            // 
            this.tabPageContractors.Controls.Add(this.tabControl2);
            this.tabPageContractors.Controls.Add(this.labelAddTasksUpdate);
            this.tabPageContractors.ImageIndex = 4;
            this.tabPageContractors.Location = new System.Drawing.Point(4, 54);
            this.tabPageContractors.Name = "tabPageContractors";
            this.tabPageContractors.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageContractors.Size = new System.Drawing.Size(1175, 722);
            this.tabPageContractors.TabIndex = 1;
            this.tabPageContractors.Text = "Contractors";
            this.tabPageContractors.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPageThisMonthsContractors);
            this.tabControl2.Controls.Add(this.tabPageAddContractors);
            this.tabControl2.ItemSize = new System.Drawing.Size(102, 50);
            this.tabControl2.Location = new System.Drawing.Point(7, 7);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(995, 900);
            this.tabControl2.TabIndex = 21;
            // 
            // tabPageThisMonthsContractors
            // 
            this.tabPageThisMonthsContractors.Controls.Add(this.buttonNoContractors);
            this.tabPageThisMonthsContractors.Controls.Add(this.labelContractorsNoTasks);
            this.tabPageThisMonthsContractors.Controls.Add(this.panelDisplayAddContractorVisit);
            this.tabPageThisMonthsContractors.Controls.Add(this.labelUpdateTodaysContractors);
            this.tabPageThisMonthsContractors.Controls.Add(this.dateTimePicker2);
            this.tabPageThisMonthsContractors.Controls.Add(this.buttonTodaysContractorsUpdate);
            this.tabPageThisMonthsContractors.Controls.Add(this.labelSelectVisitingContractor);
            this.tabPageThisMonthsContractors.Controls.Add(this.comboBox1);
            this.tabPageThisMonthsContractors.Controls.Add(this.dataGridView5);
            this.tabPageThisMonthsContractors.Location = new System.Drawing.Point(4, 54);
            this.tabPageThisMonthsContractors.Name = "tabPageThisMonthsContractors";
            this.tabPageThisMonthsContractors.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageThisMonthsContractors.Size = new System.Drawing.Size(987, 842);
            this.tabPageThisMonthsContractors.TabIndex = 0;
            this.tabPageThisMonthsContractors.Text = "Contractor Visit";
            this.tabPageThisMonthsContractors.UseVisualStyleBackColor = true;
            // 
            // buttonNoContractors
            // 
            this.buttonNoContractors.Location = new System.Drawing.Point(36, 279);
            this.buttonNoContractors.Name = "buttonNoContractors";
            this.buttonNoContractors.Size = new System.Drawing.Size(92, 23);
            this.buttonNoContractors.TabIndex = 42;
            this.buttonNoContractors.Text = "Create Items";
            this.buttonNoContractors.UseVisualStyleBackColor = true;
            this.buttonNoContractors.Click += new System.EventHandler(this.noContractors);
            // 
            // labelContractorsNoTasks
            // 
            this.labelContractorsNoTasks.AutoSize = true;
            this.labelContractorsNoTasks.Location = new System.Drawing.Point(33, 260);
            this.labelContractorsNoTasks.Name = "labelContractorsNoTasks";
            this.labelContractorsNoTasks.Size = new System.Drawing.Size(383, 17);
            this.labelContractorsNoTasks.TabIndex = 41;
            this.labelContractorsNoTasks.Text = "There are no items for this day, would you like to add one?";
            // 
            // panelDisplayAddContractorVisit
            // 
            this.panelDisplayAddContractorVisit.Controls.Add(this.buttonAddDailyContractorVisit);
            this.panelDisplayAddContractorVisit.Controls.Add(this.richTextBoxAddTodaysContractorDescription);
            this.panelDisplayAddContractorVisit.Controls.Add(this.label17);
            this.panelDisplayAddContractorVisit.Controls.Add(this.textBoxAddDailyContractorName);
            this.panelDisplayAddContractorVisit.Controls.Add(this.labelAddDailyContractorName);
            this.panelDisplayAddContractorVisit.Location = new System.Drawing.Point(23, 389);
            this.panelDisplayAddContractorVisit.Name = "panelDisplayAddContractorVisit";
            this.panelDisplayAddContractorVisit.Size = new System.Drawing.Size(697, 326);
            this.panelDisplayAddContractorVisit.TabIndex = 33;
            this.panelDisplayAddContractorVisit.Visible = false;
            // 
            // buttonAddDailyContractorVisit
            // 
            this.buttonAddDailyContractorVisit.BackColor = System.Drawing.Color.Black;
            this.buttonAddDailyContractorVisit.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonAddDailyContractorVisit.Location = new System.Drawing.Point(373, 23);
            this.buttonAddDailyContractorVisit.Name = "buttonAddDailyContractorVisit";
            this.buttonAddDailyContractorVisit.Size = new System.Drawing.Size(138, 32);
            this.buttonAddDailyContractorVisit.TabIndex = 36;
            this.buttonAddDailyContractorVisit.Text = "Add Contractors";
            this.buttonAddDailyContractorVisit.UseVisualStyleBackColor = false;
            this.buttonAddDailyContractorVisit.Click += new System.EventHandler(this.addDailyContractorVisit);
            // 
            // richTextBoxAddTodaysContractorDescription
            // 
            this.richTextBoxAddTodaysContractorDescription.Location = new System.Drawing.Point(27, 67);
            this.richTextBoxAddTodaysContractorDescription.Name = "richTextBoxAddTodaysContractorDescription";
            this.richTextBoxAddTodaysContractorDescription.Size = new System.Drawing.Size(484, 171);
            this.richTextBoxAddTodaysContractorDescription.TabIndex = 35;
            this.richTextBoxAddTodaysContractorDescription.Text = "";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(24, 48);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(137, 17);
            this.label17.TabIndex = 34;
            this.label17.Text = "Description Of Visit:";
            // 
            // textBoxAddDailyContractorName
            // 
            this.textBoxAddDailyContractorName.Location = new System.Drawing.Point(76, 23);
            this.textBoxAddDailyContractorName.Name = "textBoxAddDailyContractorName";
            this.textBoxAddDailyContractorName.Size = new System.Drawing.Size(279, 25);
            this.textBoxAddDailyContractorName.TabIndex = 33;
            // 
            // labelAddDailyContractorName
            // 
            this.labelAddDailyContractorName.AutoSize = true;
            this.labelAddDailyContractorName.Location = new System.Drawing.Point(24, 23);
            this.labelAddDailyContractorName.Name = "labelAddDailyContractorName";
            this.labelAddDailyContractorName.Size = new System.Drawing.Size(51, 17);
            this.labelAddDailyContractorName.TabIndex = 32;
            this.labelAddDailyContractorName.Text = "Name:";
            // 
            // labelUpdateTodaysContractors
            // 
            this.labelUpdateTodaysContractors.AutoSize = true;
            this.labelUpdateTodaysContractors.Location = new System.Drawing.Point(33, 327);
            this.labelUpdateTodaysContractors.Name = "labelUpdateTodaysContractors";
            this.labelUpdateTodaysContractors.Size = new System.Drawing.Size(0, 17);
            this.labelUpdateTodaysContractors.TabIndex = 32;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(6, 6);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(236, 25);
            this.dateTimePicker2.TabIndex = 26;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.loadSelectedContractorVisits);
            // 
            // buttonTodaysContractorsUpdate
            // 
            this.buttonTodaysContractorsUpdate.BackColor = System.Drawing.Color.Black;
            this.buttonTodaysContractorsUpdate.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonTodaysContractorsUpdate.Location = new System.Drawing.Point(582, 357);
            this.buttonTodaysContractorsUpdate.Name = "buttonTodaysContractorsUpdate";
            this.buttonTodaysContractorsUpdate.Size = new System.Drawing.Size(138, 32);
            this.buttonTodaysContractorsUpdate.TabIndex = 24;
            this.buttonTodaysContractorsUpdate.Text = "Update Contractors";
            this.buttonTodaysContractorsUpdate.UseVisualStyleBackColor = false;
            this.buttonTodaysContractorsUpdate.Click += new System.EventHandler(this.updateTodaysContractors);
            // 
            // labelSelectVisitingContractor
            // 
            this.labelSelectVisitingContractor.AutoSize = true;
            this.labelSelectVisitingContractor.Location = new System.Drawing.Point(20, 367);
            this.labelSelectVisitingContractor.Name = "labelSelectVisitingContractor";
            this.labelSelectVisitingContractor.Size = new System.Drawing.Size(302, 17);
            this.labelSelectVisitingContractor.TabIndex = 3;
            this.labelSelectVisitingContractor.Text = "Select a contractor to add details of their visit:";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            " "});
            this.comboBox1.Location = new System.Drawing.Point(328, 364);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(166, 25);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.DropDownClosed += new System.EventHandler(this.contractorSelected);
            // 
            // dataGridView5
            // 
            this.dataGridView5.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.VisitDate});
            this.dataGridView5.Location = new System.Drawing.Point(23, 53);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(697, 305);
            this.dataGridView5.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "company";
            this.dataGridViewTextBoxColumn3.HeaderText = "Company";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "description";
            this.dataGridViewTextBoxColumn4.HeaderText = "Description";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // VisitDate
            // 
            this.VisitDate.DataPropertyName = "date";
            this.VisitDate.HeaderText = "Date";
            this.VisitDate.Name = "VisitDate";
            this.VisitDate.ReadOnly = true;
            // 
            // tabPageAddContractors
            // 
            this.tabPageAddContractors.AutoScroll = true;
            this.tabPageAddContractors.Controls.Add(this.labelUpdateContractors);
            this.tabPageAddContractors.Controls.Add(this.buttonuupdateContractors);
            this.tabPageAddContractors.Controls.Add(this.labelAddContractorsHeader);
            this.tabPageAddContractors.Controls.Add(this.panel3);
            this.tabPageAddContractors.Controls.Add(this.dataGridView4);
            this.tabPageAddContractors.Location = new System.Drawing.Point(4, 54);
            this.tabPageAddContractors.Name = "tabPageAddContractors";
            this.tabPageAddContractors.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAddContractors.Size = new System.Drawing.Size(987, 842);
            this.tabPageAddContractors.TabIndex = 1;
            this.tabPageAddContractors.Text = "Manage Contractors";
            this.tabPageAddContractors.UseVisualStyleBackColor = true;
            // 
            // labelUpdateContractors
            // 
            this.labelUpdateContractors.AutoSize = true;
            this.labelUpdateContractors.Location = new System.Drawing.Point(21, 264);
            this.labelUpdateContractors.Name = "labelUpdateContractors";
            this.labelUpdateContractors.Size = new System.Drawing.Size(0, 17);
            this.labelUpdateContractors.TabIndex = 24;
            // 
            // buttonuupdateContractors
            // 
            this.buttonuupdateContractors.BackColor = System.Drawing.Color.Black;
            this.buttonuupdateContractors.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonuupdateContractors.Location = new System.Drawing.Point(569, 256);
            this.buttonuupdateContractors.Name = "buttonuupdateContractors";
            this.buttonuupdateContractors.Size = new System.Drawing.Size(138, 32);
            this.buttonuupdateContractors.TabIndex = 23;
            this.buttonuupdateContractors.Text = "Save Contractors";
            this.buttonuupdateContractors.UseVisualStyleBackColor = false;
            this.buttonuupdateContractors.Click += new System.EventHandler(this.updateContractors);
            // 
            // labelAddContractorsHeader
            // 
            this.labelAddContractorsHeader.AutoSize = true;
            this.labelAddContractorsHeader.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddContractorsHeader.Location = new System.Drawing.Point(21, 7);
            this.labelAddContractorsHeader.Name = "labelAddContractorsHeader";
            this.labelAddContractorsHeader.Size = new System.Drawing.Size(331, 16);
            this.labelAddContractorsHeader.TabIndex = 22;
            this.labelAddContractorsHeader.Text = "View all your contractors and add or manage them";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel3.Controls.Add(this.comboBoxAddContractorPeriodOfTime);
            this.panel3.Controls.Add(this.label30);
            this.panel3.Controls.Add(this.textBoxAddContractorAmountOfTimeBetweenVisits);
            this.panel3.Controls.Add(this.checkBoxAddContractorEveryXMonths);
            this.panel3.Controls.Add(this.textBoxAddContractorPhone);
            this.panel3.Controls.Add(this.label29);
            this.panel3.Controls.Add(this.textBoxAddContractorCertNumber);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.textBoxAddContractorNextVisit);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.labelAddContractorValidate);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.richTextBoxAddContractorDescription);
            this.panel3.Controls.Add(this.textBoxAddContractorCompany);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.buttonAddContractor);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.textBoxAddContractorName);
            this.panel3.Location = new System.Drawing.Point(21, 283);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(708, 517);
            this.panel3.TabIndex = 21;
            // 
            // comboBoxAddContractorPeriodOfTime
            // 
            this.comboBoxAddContractorPeriodOfTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAddContractorPeriodOfTime.FormattingEnabled = true;
            this.comboBoxAddContractorPeriodOfTime.Items.AddRange(new object[] {
            "week",
            "month"});
            this.comboBoxAddContractorPeriodOfTime.Location = new System.Drawing.Point(15, 71);
            this.comboBoxAddContractorPeriodOfTime.Name = "comboBoxAddContractorPeriodOfTime";
            this.comboBoxAddContractorPeriodOfTime.Size = new System.Drawing.Size(121, 25);
            this.comboBoxAddContractorPeriodOfTime.TabIndex = 36;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(14, 50);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(49, 17);
            this.label30.TabIndex = 35;
            this.label30.Text = "Every:";
            // 
            // textBoxAddContractorAmountOfTimeBetweenVisits
            // 
            this.textBoxAddContractorAmountOfTimeBetweenVisits.Location = new System.Drawing.Point(61, 44);
            this.textBoxAddContractorAmountOfTimeBetweenVisits.Name = "textBoxAddContractorAmountOfTimeBetweenVisits";
            this.textBoxAddContractorAmountOfTimeBetweenVisits.Size = new System.Drawing.Size(108, 25);
            this.textBoxAddContractorAmountOfTimeBetweenVisits.TabIndex = 34;
            // 
            // checkBoxAddContractorEveryXMonths
            // 
            this.checkBoxAddContractorEveryXMonths.AutoSize = true;
            this.checkBoxAddContractorEveryXMonths.Location = new System.Drawing.Point(15, 18);
            this.checkBoxAddContractorEveryXMonths.Name = "checkBoxAddContractorEveryXMonths";
            this.checkBoxAddContractorEveryXMonths.Size = new System.Drawing.Size(142, 21);
            this.checkBoxAddContractorEveryXMonths.TabIndex = 33;
            this.checkBoxAddContractorEveryXMonths.Text = "Visits at set time?";
            this.checkBoxAddContractorEveryXMonths.UseVisualStyleBackColor = true;
            // 
            // textBoxAddContractorPhone
            // 
            this.textBoxAddContractorPhone.Location = new System.Drawing.Point(123, 167);
            this.textBoxAddContractorPhone.Name = "textBoxAddContractorPhone";
            this.textBoxAddContractorPhone.Size = new System.Drawing.Size(347, 25);
            this.textBoxAddContractorPhone.TabIndex = 32;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 168);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(110, 17);
            this.label29.TabIndex = 31;
            this.label29.Text = "Phone Number:";
            // 
            // textBoxAddContractorCertNumber
            // 
            this.textBoxAddContractorCertNumber.Location = new System.Drawing.Point(155, 233);
            this.textBoxAddContractorCertNumber.Name = "textBoxAddContractorCertNumber";
            this.textBoxAddContractorCertNumber.Size = new System.Drawing.Size(315, 25);
            this.textBoxAddContractorCertNumber.TabIndex = 30;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(9, 233);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(145, 17);
            this.label28.TabIndex = 29;
            this.label28.Text = "Certification Number:";
            // 
            // textBoxAddContractorNextVisit
            // 
            this.textBoxAddContractorNextVisit.Location = new System.Drawing.Point(155, 198);
            this.textBoxAddContractorNextVisit.Name = "textBoxAddContractorNextVisit";
            this.textBoxAddContractorNextVisit.Size = new System.Drawing.Size(315, 25);
            this.textBoxAddContractorNextVisit.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 198);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(162, 17);
            this.label14.TabIndex = 27;
            this.label14.Text = "Next Visit(dd-mm-yyyy):";
            // 
            // labelAddContractorValidate
            // 
            this.labelAddContractorValidate.AutoSize = true;
            this.labelAddContractorValidate.Location = new System.Drawing.Point(10, 281);
            this.labelAddContractorValidate.Name = "labelAddContractorValidate";
            this.labelAddContractorValidate.Size = new System.Drawing.Size(0, 17);
            this.labelAddContractorValidate.TabIndex = 26;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 278);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 17);
            this.label13.TabIndex = 22;
            this.label13.Text = "Description:";
            // 
            // richTextBoxAddContractorDescription
            // 
            this.richTextBoxAddContractorDescription.Location = new System.Drawing.Point(123, 278);
            this.richTextBoxAddContractorDescription.Name = "richTextBoxAddContractorDescription";
            this.richTextBoxAddContractorDescription.Size = new System.Drawing.Size(468, 185);
            this.richTextBoxAddContractorDescription.TabIndex = 21;
            this.richTextBoxAddContractorDescription.Text = "";
            // 
            // textBoxAddContractorCompany
            // 
            this.textBoxAddContractorCompany.Location = new System.Drawing.Point(123, 136);
            this.textBoxAddContractorCompany.Name = "textBoxAddContractorCompany";
            this.textBoxAddContractorCompany.Size = new System.Drawing.Size(347, 25);
            this.textBoxAddContractorCompany.TabIndex = 20;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 137);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 17);
            this.label12.TabIndex = 19;
            this.label12.Text = "Company:";
            // 
            // buttonAddContractor
            // 
            this.buttonAddContractor.BackColor = System.Drawing.Color.Black;
            this.buttonAddContractor.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonAddContractor.Location = new System.Drawing.Point(516, 247);
            this.buttonAddContractor.Name = "buttonAddContractor";
            this.buttonAddContractor.Size = new System.Drawing.Size(75, 25);
            this.buttonAddContractor.TabIndex = 18;
            this.buttonAddContractor.Text = "Enter Task";
            this.buttonAddContractor.UseVisualStyleBackColor = false;
            this.buttonAddContractor.Click += new System.EventHandler(this.AddContractors);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(260, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(203, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Add a contractor/ service provider";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 88);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 17);
            this.label10.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 108);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(124, 17);
            this.label11.TabIndex = 17;
            this.label11.Text = "Contractor Name:";
            // 
            // textBoxAddContractorName
            // 
            this.textBoxAddContractorName.Location = new System.Drawing.Point(123, 108);
            this.textBoxAddContractorName.Name = "textBoxAddContractorName";
            this.textBoxAddContractorName.Size = new System.Drawing.Size(347, 25);
            this.textBoxAddContractorName.TabIndex = 16;
            // 
            // dataGridView4
            // 
            this.dataGridView4.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.company,
            this.description,
            this.Certification_Number,
            this.vistitFrequency});
            this.dataGridView4.Location = new System.Drawing.Point(21, 26);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(708, 230);
            this.dataGridView4.TabIndex = 0;
            // 
            // name
            // 
            this.name.DataPropertyName = "name";
            this.name.HeaderText = "Name";
            this.name.Name = "name";
            // 
            // company
            // 
            this.company.DataPropertyName = "company";
            this.company.HeaderText = "Company";
            this.company.Name = "company";
            // 
            // description
            // 
            this.description.DataPropertyName = "description";
            this.description.HeaderText = "Description";
            this.description.Name = "description";
            // 
            // Certification_Number
            // 
            this.Certification_Number.DataPropertyName = "certification_no";
            this.Certification_Number.HeaderText = "Cert Number";
            this.Certification_Number.Name = "Certification_Number";
            // 
            // vistitFrequency
            // 
            this.vistitFrequency.DataPropertyName = "next_visit";
            this.vistitFrequency.HeaderText = "Next Visit";
            this.vistitFrequency.Name = "vistitFrequency";
            // 
            // labelAddTasksUpdate
            // 
            this.labelAddTasksUpdate.AutoSize = true;
            this.labelAddTasksUpdate.Location = new System.Drawing.Point(13, 421);
            this.labelAddTasksUpdate.Name = "labelAddTasksUpdate";
            this.labelAddTasksUpdate.Size = new System.Drawing.Size(0, 17);
            this.labelAddTasksUpdate.TabIndex = 20;
            // 
            // tabPageExportLists
            // 
            this.tabPageExportLists.Controls.Add(this.SaveSelection);
            this.tabPageExportLists.ImageIndex = 5;
            this.tabPageExportLists.Location = new System.Drawing.Point(4, 54);
            this.tabPageExportLists.Name = "tabPageExportLists";
            this.tabPageExportLists.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageExportLists.Size = new System.Drawing.Size(1175, 722);
            this.tabPageExportLists.TabIndex = 2;
            this.tabPageExportLists.Text = "Save Lists";
            this.tabPageExportLists.UseVisualStyleBackColor = true;
            // 
            // SaveSelection
            // 
            this.SaveSelection.Controls.Add(this.tabPageSaveToExcel);
            this.SaveSelection.Location = new System.Drawing.Point(7, 7);
            this.SaveSelection.Name = "SaveSelection";
            this.SaveSelection.SelectedIndex = 0;
            this.SaveSelection.Size = new System.Drawing.Size(995, 900);
            this.SaveSelection.TabIndex = 0;
            // 
            // tabPageSaveToExcel
            // 
            this.tabPageSaveToExcel.Controls.Add(this.buttonDebug);
            this.tabPageSaveToExcel.Controls.Add(this.label25);
            this.tabPageSaveToExcel.Controls.Add(this.comboBoxChooseSaveList);
            this.tabPageSaveToExcel.Controls.Add(this.progressBar1);
            this.tabPageSaveToExcel.Controls.Add(this.exportCSVValidate);
            this.tabPageSaveToExcel.Controls.Add(this.buttonSaveToCSV);
            this.tabPageSaveToExcel.Controls.Add(this.label8);
            this.tabPageSaveToExcel.Controls.Add(this.label7);
            this.tabPageSaveToExcel.Controls.Add(this.textBoxSaveToCSVEndYear);
            this.tabPageSaveToExcel.Controls.Add(this.textBoxSaveToCSVStartYear);
            this.tabPageSaveToExcel.Controls.Add(this.textBoxSaveToCSVEndDay);
            this.tabPageSaveToExcel.Controls.Add(this.textBoxSaveToCSVStartDate);
            this.tabPageSaveToExcel.Controls.Add(this.label6);
            this.tabPageSaveToExcel.Controls.Add(this.label5);
            this.tabPageSaveToExcel.Controls.Add(this.label4);
            this.tabPageSaveToExcel.Controls.Add(this.label3);
            this.tabPageSaveToExcel.Controls.Add(this.comboBoxSaveToCSVEndMonth);
            this.tabPageSaveToExcel.Controls.Add(this.comboBoxSaveToCSVStartMonth);
            this.tabPageSaveToExcel.Controls.Add(this.label2);
            this.tabPageSaveToExcel.Location = new System.Drawing.Point(4, 26);
            this.tabPageSaveToExcel.Name = "tabPageSaveToExcel";
            this.tabPageSaveToExcel.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSaveToExcel.Size = new System.Drawing.Size(987, 870);
            this.tabPageSaveToExcel.TabIndex = 0;
            this.tabPageSaveToExcel.Text = "Export Lists to Excel";
            this.tabPageSaveToExcel.UseVisualStyleBackColor = true;
            // 
            // buttonDebug
            // 
            this.buttonDebug.ImageIndex = 6;
            this.buttonDebug.ImageList = this.imageListNavIcons;
            this.buttonDebug.Location = new System.Drawing.Point(910, 10);
            this.buttonDebug.Name = "buttonDebug";
            this.buttonDebug.Size = new System.Drawing.Size(75, 23);
            this.buttonDebug.TabIndex = 41;
            this.buttonDebug.UseVisualStyleBackColor = true;
            this.buttonDebug.Click += new System.EventHandler(this.launchDegugTerminal);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(87, 225);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(113, 17);
            this.label25.TabIndex = 40;
            this.label25.Text = "Save which list?";
            // 
            // comboBoxChooseSaveList
            // 
            this.comboBoxChooseSaveList.FormattingEnabled = true;
            this.comboBoxChooseSaveList.Items.AddRange(new object[] {
            "Tasks",
            "Contractor Visits",
            "Contractors",
            "Providers",
            "Maintenence",
            "Inventory"});
            this.comboBoxChooseSaveList.Location = new System.Drawing.Point(206, 222);
            this.comboBoxChooseSaveList.Name = "comboBoxChooseSaveList";
            this.comboBoxChooseSaveList.Size = new System.Drawing.Size(121, 25);
            this.comboBoxChooseSaveList.TabIndex = 39;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(9, 283);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(318, 23);
            this.progressBar1.TabIndex = 37;
            // 
            // exportCSVValidate
            // 
            this.exportCSVValidate.AutoSize = true;
            this.exportCSVValidate.Location = new System.Drawing.Point(55, 309);
            this.exportCSVValidate.Name = "exportCSVValidate";
            this.exportCSVValidate.Size = new System.Drawing.Size(0, 17);
            this.exportCSVValidate.TabIndex = 36;
            // 
            // buttonSaveToCSV
            // 
            this.buttonSaveToCSV.BackColor = System.Drawing.Color.Black;
            this.buttonSaveToCSV.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonSaveToCSV.Location = new System.Drawing.Point(169, 253);
            this.buttonSaveToCSV.Name = "buttonSaveToCSV";
            this.buttonSaveToCSV.Size = new System.Drawing.Size(158, 25);
            this.buttonSaveToCSV.TabIndex = 35;
            this.buttonSaveToCSV.Text = "Save To MS Excel";
            this.buttonSaveToCSV.UseVisualStyleBackColor = false;
            this.buttonSaveToCSV.Click += new System.EventHandler(this.clickExportTasksToExcel);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(282, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 17);
            this.label8.TabIndex = 34;
            this.label8.Text = "Year";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(191, 110);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 17);
            this.label7.TabIndex = 33;
            this.label7.Text = "Month ";
            // 
            // textBoxSaveToCSVEndYear
            // 
            this.textBoxSaveToCSVEndYear.Location = new System.Drawing.Point(244, 178);
            this.textBoxSaveToCSVEndYear.Name = "textBoxSaveToCSVEndYear";
            this.textBoxSaveToCSVEndYear.Size = new System.Drawing.Size(72, 25);
            this.textBoxSaveToCSVEndYear.TabIndex = 32;
            // 
            // textBoxSaveToCSVStartYear
            // 
            this.textBoxSaveToCSVStartYear.Location = new System.Drawing.Point(244, 129);
            this.textBoxSaveToCSVStartYear.Name = "textBoxSaveToCSVStartYear";
            this.textBoxSaveToCSVStartYear.Size = new System.Drawing.Size(72, 25);
            this.textBoxSaveToCSVStartYear.TabIndex = 31;
            // 
            // textBoxSaveToCSVEndDay
            // 
            this.textBoxSaveToCSVEndDay.Location = new System.Drawing.Point(58, 178);
            this.textBoxSaveToCSVEndDay.Name = "textBoxSaveToCSVEndDay";
            this.textBoxSaveToCSVEndDay.Size = new System.Drawing.Size(52, 25);
            this.textBoxSaveToCSVEndDay.TabIndex = 30;
            // 
            // textBoxSaveToCSVStartDate
            // 
            this.textBoxSaveToCSVStartDate.Location = new System.Drawing.Point(58, 129);
            this.textBoxSaveToCSVStartDate.Name = "textBoxSaveToCSVStartDate";
            this.textBoxSaveToCSVStartDate.Size = new System.Drawing.Size(52, 25);
            this.textBoxSaveToCSVStartDate.TabIndex = 29;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 17);
            this.label6.TabIndex = 28;
            this.label6.Text = "From: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(541, 17);
            this.label5.TabIndex = 27;
            this.label5.Text = "Choose tasks from a period in time to save them to a Microsoft Excel Spreadsheet";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 17);
            this.label4.TabIndex = 26;
            this.label4.Text = "To:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(64, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 17);
            this.label3.TabIndex = 25;
            this.label3.Text = "Day";
            // 
            // comboBoxSaveToCSVEndMonth
            // 
            this.comboBoxSaveToCSVEndMonth.FormattingEnabled = true;
            this.comboBoxSaveToCSVEndMonth.Items.AddRange(new object[] {
            "January",
            "Febuary",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.comboBoxSaveToCSVEndMonth.Location = new System.Drawing.Point(116, 178);
            this.comboBoxSaveToCSVEndMonth.Name = "comboBoxSaveToCSVEndMonth";
            this.comboBoxSaveToCSVEndMonth.Size = new System.Drawing.Size(121, 25);
            this.comboBoxSaveToCSVEndMonth.TabIndex = 24;
            // 
            // comboBoxSaveToCSVStartMonth
            // 
            this.comboBoxSaveToCSVStartMonth.FormattingEnabled = true;
            this.comboBoxSaveToCSVStartMonth.Items.AddRange(new object[] {
            "January",
            "Febuary",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.comboBoxSaveToCSVStartMonth.Location = new System.Drawing.Point(116, 129);
            this.comboBoxSaveToCSVStartMonth.Name = "comboBoxSaveToCSVStartMonth";
            this.comboBoxSaveToCSVStartMonth.Size = new System.Drawing.Size(121, 25);
            this.comboBoxSaveToCSVStartMonth.TabIndex = 23;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(358, 16);
            this.label2.TabIndex = 22;
            this.label2.Text = "Save all the tasks you have completed each month";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.SlateGray;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Location = new System.Drawing.Point(-1, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(125, 780);
            this.panel7.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::MaintenenceGUI.Properties.Resources.tweed;
            this.ClientSize = new System.Drawing.Size(979, 723);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Maintanence";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.establishConnection);
            this.panel1.ResumeLayout(false);
            this.menuStripLeftNav.ResumeLayout(false);
            this.menuStripLeftNav.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPageDashboard.ResumeLayout(false);
            this.tabPageTaskList.ResumeLayout(false);
            this.tabPageTaskList.PerformLayout();
            this.TasksSelection.ResumeLayout(false);
            this.tabPageChooseTodaysTasks.ResumeLayout(false);
            this.tabPageChooseTodaysTasks.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPageChooseWeeklyTasks.ResumeLayout(false);
            this.tabPageChooseWeeklyTasks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabPageChooseAddTasks.ResumeLayout(false);
            this.tabPageChooseAddTasks.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPageInventoryMaintenence.ResumeLayout(false);
            this.tabPageInventoryMaintenence.PerformLayout();
            this.tabControl3.ResumeLayout(false);
            this.tabPageItemMaintenence.ResumeLayout(false);
            this.tabPageItemMaintenence.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.tabPageInventory.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            this.tabPageMaintenanceLegislation.ResumeLayout(false);
            this.tabPageMaintenanceLegislation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).EndInit();
            this.tabPageContractors.ResumeLayout(false);
            this.tabPageContractors.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPageThisMonthsContractors.ResumeLayout(false);
            this.tabPageThisMonthsContractors.PerformLayout();
            this.panelDisplayAddContractorVisit.ResumeLayout(false);
            this.panelDisplayAddContractorVisit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.tabPageAddContractors.ResumeLayout(false);
            this.tabPageAddContractors.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.tabPageExportLists.ResumeLayout(false);
            this.SaveSelection.ResumeLayout(false);
            this.tabPageSaveToExcel.ResumeLayout(false);
            this.tabPageSaveToExcel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageTaskList;
        private System.Windows.Forms.TabPage tabPageContractors;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage tabPageExportLists;
        private System.Windows.Forms.Label labelTodaysTasksFileNotFound;
        private System.Windows.Forms.TabPage tabPageInventoryMaintenence;
        private System.Windows.Forms.Label labelTodaysTasksUpdate;
        private System.Windows.Forms.Label labelAddTasksUpdate;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.TabControl TasksSelection;
        private System.Windows.Forms.TabPage tabPageChooseTodaysTasks;
        private System.Windows.Forms.TabPage tabPageChooseWeeklyTasks;
        private System.Windows.Forms.Button buttonTodaysTasksUpdate;
        private System.Windows.Forms.Label labelTodaysDate;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label labelExtraTasksDate;
        private System.Windows.Forms.Label labelExtraTasksHeader;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn weekly_or_monthly;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn date_completed;
        private System.Windows.Forms.TabPage tabPageChooseAddTasks;
        private System.Windows.Forms.Label labelFutureTasksHeader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button buttonAddTasksUpdate;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonFutureTaskAdd;
        private System.Windows.Forms.Label labelFutureTasksHeader1;
        private System.Windows.Forms.Label labelAddTaskValidate;
        private System.Windows.Forms.Label labelFutureTaskName;
        private System.Windows.Forms.RadioButton radioButtonDailyTask;
        private System.Windows.Forms.TextBox textBoxFutureName;
        private System.Windows.Forms.RadioButton radioButtonWeeklyTask;
        private System.Windows.Forms.RadioButton radioButtonMonthlyTask;
        private System.Windows.Forms.Label labelExtraTasksUpdate;
        private System.Windows.Forms.TabControl SaveSelection;
        private System.Windows.Forms.TabPage tabPageSaveToExcel;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label exportCSVValidate;
        private System.Windows.Forms.Button buttonSaveToCSV;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxSaveToCSVEndYear;
        private System.Windows.Forms.TextBox textBoxSaveToCSVStartYear;
        private System.Windows.Forms.TextBox textBoxSaveToCSVEndDay;
        private System.Windows.Forms.TextBox textBoxSaveToCSVStartDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxSaveToCSVEndMonth;
        private System.Windows.Forms.ComboBox comboBoxSaveToCSVStartMonth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button buttonTodaysTasksAddExtraTask;
        private System.Windows.Forms.Label labelTodaysTasksAddExtraTashHeader;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelTodaysTasksExtraTaskName;
        private System.Windows.Forms.TextBox textBoxTodaysTasksExtraTask;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RichTextBox richTextBoxTodaysTaskAddDescrition;
        private System.Windows.Forms.DataGridViewTextBoxColumn task;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaskDescription;
        private System.Windows.Forms.DataGridViewCheckBoxColumn completed;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.Label labelAddOneOffTaskValidate;
        private System.Windows.Forms.CheckBox checkBoxAddOneOffTask;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPageItemMaintenence;
        private System.Windows.Forms.TabPage tabPageInventory;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textBoxAddItemMaintenceRoom;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.RichTextBox richTextBoxAddItemMaintenceDescription;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxAddItemMaintenenceItem;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.TextBox textBoxItemMaintenenceArea;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button buttonUpdateInventory;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox textBoxAddInventoryArea;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxAddInventoryQuantity;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button buttonAddInventroyItem;
        private System.Windows.Forms.TextBox textBoxAddInventoryItem;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox comboBoxChooseSaveList;
        private System.Windows.Forms.Button buttonNoDailyTasks;
        private System.Windows.Forms.Label labelNoDailyItems;
        private System.Windows.Forms.Button buttonNoMaintenenceItems;
        private System.Windows.Forms.Label labelMaintenenceNoTasks;
        private System.Windows.Forms.RadioButton radioButtonSixMonthly;
        private System.Windows.Forms.RadioButton radioButtonThreeMonthly;
        private System.Windows.Forms.RadioButton radioButtonTwoMonthly;
        private System.Windows.Forms.DataGridViewTextBoxColumn task_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn frequency;
        private System.Windows.Forms.Label labelAddInventoryValidate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Item;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Room;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.MenuStrip menuStripLeftNav;
        private System.Windows.Forms.ToolStripMenuItem toolStripHeader;
        private System.Windows.Forms.ToolStripMenuItem taskListToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem todaysTasksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem weeklyTasksToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem manageTasksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maintenenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryMaintenenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryStockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contractorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contractorVisitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem manageContractorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveListsToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPageThisMonthsContractors;
        private System.Windows.Forms.Button buttonNoContractors;
        private System.Windows.Forms.Label labelContractorsNoTasks;
        private System.Windows.Forms.Panel panelDisplayAddContractorVisit;
        private System.Windows.Forms.Button buttonAddDailyContractorVisit;
        private System.Windows.Forms.RichTextBox richTextBoxAddTodaysContractorDescription;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxAddDailyContractorName;
        private System.Windows.Forms.Label labelAddDailyContractorName;
        private System.Windows.Forms.Label labelUpdateTodaysContractors;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Button buttonTodaysContractorsUpdate;
        private System.Windows.Forms.Label labelSelectVisitingContractor;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn VisitDate;
        private System.Windows.Forms.TabPage tabPageAddContractors;
        private System.Windows.Forms.Label labelUpdateContractors;
        private System.Windows.Forms.Button buttonuupdateContractors;
        private System.Windows.Forms.Label labelAddContractorsHeader;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox comboBoxAddContractorPeriodOfTime;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBoxAddContractorAmountOfTimeBetweenVisits;
        private System.Windows.Forms.CheckBox checkBoxAddContractorEveryXMonths;
        private System.Windows.Forms.TextBox textBoxAddContractorPhone;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBoxAddContractorCertNumber;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBoxAddContractorNextVisit;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelAddContractorValidate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RichTextBox richTextBoxAddContractorDescription;
        private System.Windows.Forms.TextBox textBoxAddContractorCompany;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button buttonAddContractor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxAddContractorName;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn company;
        private System.Windows.Forms.DataGridViewTextBoxColumn description;
        private System.Windows.Forms.DataGridViewTextBoxColumn Certification_Number;
        private System.Windows.Forms.DataGridViewTextBoxColumn vistitFrequency;
        private System.Windows.Forms.Button buttonAddItemMaintenence;
        private System.Windows.Forms.Button buttonExtraTasksUpdate;
        private System.Windows.Forms.TabPage tabPageMaintenanceLegislation;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button buttonAddServiceProvider;
        private System.Windows.Forms.TextBox textBoxAddProviderDateCompleted;
        private System.Windows.Forms.TextBox textBoxAddProviderDueDate;
        private System.Windows.Forms.TextBox textBoxAddProvidersName;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.DataGridView dataGridView8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn date_completed1;
        private System.Windows.Forms.DataGridViewTextBoxColumn date_due1;
        private System.Windows.Forms.Button buttonUpdateServiceProviders;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLegilativeMaintenance;
        private System.Windows.Forms.Label labelAddProviderValidate;
        private System.Windows.Forms.TabPage tabPageDashboard;
        private System.Windows.Forms.ImageList imageListNavIcons;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button buttonDebug;
        private System.Windows.Forms.RichTextBox richTextBoxDashTasks;
    }
}

