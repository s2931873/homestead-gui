﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Xml;
using AutoUpdaterDotNET;

namespace MaintenenceGUI
{
    public partial class Form1 : Form
    {
        private static string fileNameDate = DateTime.Now.ToString("d-MM-yyyy");
        private static string selectedDate;


        public Form1()
        {
            InitializeComponent();
            try
            {
                AutoUpdater.Start("http://www.matthewmarillac.com/mmm/versioninfo.xml");
            }
            catch (Exception err)
            {
                MessageBox.Show("Updater failed!");
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
        
            }
        }

        protected override CreateParams CreateParams
        {
            //fixes flikering tabs error
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        private void establishConnection(object sender, EventArgs e)
        {

            //first step is to load startup config
            applicationCore apc = new applicationCore(fileNameDate);
            apc.startUp();
            //load default values for form elements
            textBoxSaveToCSVEndDay.Text = DateTime.Now.ToString("dd");
            comboBoxSaveToCSVEndMonth.SelectedItem = comboBoxSaveToCSVEndMonth.Text = DateTime.Now.ToString("MMMM");
            textBoxSaveToCSVEndYear.Text = DateTime.Now.ToString("yyyy");
            progressBar1.Visible = false;
            labelNoDailyItems.Visible = false;
            buttonNoDailyTasks.Visible = false;
            labelContractorsNoTasks.Visible = false;
            buttonNoContractors.Visible = false;
            labelMaintenenceNoTasks.Visible = false;
            buttonNoMaintenenceItems.Visible = false;
            //load the dataGridView Displays
            selectedDate = fileNameDate;
            loadTodaysTasks(selectedDate);
            loadExtraTasks();
            loadAllTheTasks();
            loadDailyContractors(fileNameDate);
            loadAllContractors();
            loadAllProviders();
            loadAllMaintenceItems(fileNameDate);
            loadInventoryItems();
            //load dash
            loadDashboard();

        }

        private void loadSelectedDailyTask(object sender, EventArgs e)
        {
            selectedDate = this.dateTimePicker1.Value.ToString("d-MM-yyyy");
            applicationCore ap = new applicationCore(selectedDate);
            dataBase db = new dataBase();
            if (!File.Exists(ap.path + selectedDate + @".xml"))
            {
                selectedDate = fileNameDate;
                labelNoDailyItems.Visible = true;
                buttonNoDailyTasks.Visible = true;

            }
            else
            {
                labelNoDailyItems.Visible = false;
                buttonNoDailyTasks.Visible = false;
            }


        }

        private void noTasks(object sender, EventArgs e)
        {
            labelNoDailyItems.Visible = false;
            buttonNoDailyTasks.Visible = false;
            dataBase db = new dataBase();
            applicationCore ap = new applicationCore(selectedDate);
            if (db.create("root", ap.path + selectedDate + ".xml"))
            {
                ap.createTodaysTasks();
                loadTodaysTasks(selectedDate);
            }
        }

        private void loadInventoryItems()
        {
            try
            {
                applicationCore ap = new applicationCore(fileNameDate);
                DataSet ds = new DataSet();


                ds.ReadXml(ap.path + @"maintenence\inventory\" + "list.xml").ToString();

                dataGridView7.DataSource = ds;
                if (ds.Tables.Count > 0)
                {
                    dataGridView7.DataMember = "items";
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                //error warning
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        private void updateInventory(object sender, EventArgs e)
        {

            applicationCore ap = new applicationCore(fileNameDate);
            datasetToXml(ap.path + @"maintenence\inventory\" + "list.xml", 7);

        }

        private void addItemMaintenence(object sender, EventArgs e)
        {
            try
            {
                dataBase db = new dataBase();
                applicationCore ap = new applicationCore(fileNameDate);
                ArrayList element = new ArrayList();
                //add the xml elements names
                element.Add("item");
                element.Add("area");
                element.Add("quantity");
                ArrayList text = new ArrayList();
                text.Add(textBoxAddInventoryItem.Text);
                text.Add(textBoxAddInventoryArea.Text);
                text.Add(textBoxAddInventoryQuantity.Text);
                labelAddInventoryValidate.Text = textBoxAddInventoryItem.Text + " succesfully added";
                textBoxAddInventoryItem.Clear();
                db.write(ap.path + @"maintenence\inventory\" + "list.xml",
                                element, text, "items");
                loadInventoryItems();
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }

        }

        private void loadSelectedMaintenenceItems(object sender, EventArgs e)
        {
            selectedDate = this.dateTimePicker3.Value.ToString("d-MM-yyyy");
            applicationCore ap = new applicationCore(selectedDate);
            dataBase db = new dataBase();
            if (!File.Exists(ap.path + @"maintenence\" + selectedDate + @".xml"))
            {
                labelMaintenenceNoTasks.Visible = true;
                buttonNoMaintenenceItems.Visible = true;
            }
            else
            {
                selectedDate = fileNameDate;
                labelMaintenenceNoTasks.Visible = false;
                buttonNoMaintenenceItems.Visible = false;
            }


        }

        private void noMaintenence(object sender, EventArgs e)
        {
            applicationCore ap = new applicationCore(selectedDate);
            dataBase db = new dataBase();
            if (db.create("item_maintenence", ap.path + @"maintenence\" + selectedDate + ".xml"))
            {
                ap.createTodaysTasks();
                loadAllMaintenceItems(selectedDate);
            }
            labelMaintenenceNoTasks.Visible = false;
            buttonNoMaintenenceItems.Visible = false;
        }

        public void loadTodaysTasks(string date)
        {
            try
            {
                applicationCore ap = new applicationCore(fileNameDate);
                DataSet ds = new DataSet();

                ds.ReadXml(ap.path + date + ".xml").ToString();

                dataGridView1.DataSource = ds;
                if (ds.Tables.Count > 0)
                {
                    dataGridView1.DataMember += "tasks";
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                //error warning
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public void loadExtraTasks()
        {
            try
            {
                applicationCore ap = new applicationCore(fileNameDate);
                DataSet ds = new DataSet();


                ds.ReadXml(ap.path + @"extra\" + DateTime.Now.ToString("MMMM-yyyy") + ".xml").ToString();

                dataGridView3.DataSource = ds;
                if (ds.Tables.Count > 0)
                {
                    dataGridView3.DataMember = "extra_tasks";
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                //error warning
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        private void loadSelectedContractorVisits(object sender, EventArgs e)
        {
            selectedDate = this.dateTimePicker2.Value.ToString("d-MM-yyyy");
            applicationCore ap = new applicationCore(selectedDate);
            dataBase db = new dataBase();
            if (!File.Exists(ap.path + @"contractors\" + selectedDate + @".xml"))
            {
                labelContractorsNoTasks.Visible = true;
                buttonNoContractors.Visible = true;
            }
            else
            {
                selectedDate = fileNameDate;
                labelContractorsNoTasks.Visible = false;
                buttonNoContractors.Visible = false;
            }

            loadDailyContractors(selectedDate);
        }

        private void noContractors(object sender, EventArgs e)
        {
            dataBase db = new dataBase();
            applicationCore ap = new applicationCore(selectedDate);
            db.create("contractors_today", ap.path + @"contractors\" + selectedDate + @".xml");
            labelContractorsNoTasks.Visible = false;
            buttonNoContractors.Visible = false;
        }

        private void loadDailyContractors(string date)
        {

            try
            {
                applicationCore ap = new applicationCore(date);
                DataSet ds1 = new DataSet();


                ds1.ReadXml(ap.path + @"\contractors\lists\contractors.xml");
                //load all created contractors into combo boxs
                comboBox1.DataSource = ds1.Tables["contractor"];
                if (ds1.Tables.Count > 0)
                {
                    comboBox1.DisplayMember = "company";
                }

                //load todays contractors into box
                DataSet ds2 = new DataSet();
                ds2.ReadXml(ap.path + @"\contractors\" + date + ".xml");
                if (ds2.Tables.Count > 0)
                {
                    dataGridView5.DataSource = ds2;
                    dataGridView5.DataMember = "visitor";
                }
                else
                {
                    dataGridView5.DataSource = null;
                    labelUpdateTodaysContractors.Text = "There are no records for this day, add one bellow";
                }


            }
            catch (System.IO.FileNotFoundException)
            {
                //error warning
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        private void contractorSelected(object sender, EventArgs e)
        {

            //fill out aditional information about contractor
            if (comboBox1.Text.Length > 0 && comboBox1.Text != "")
            {
                panelDisplayAddContractorVisit.Visible = true;
                applicationCore ap = new applicationCore(selectedDate);
                textBoxAddDailyContractorName.Text = ap.getContractorName(comboBox1.Text.ToString());
            }

        }

        private void addDailyContractorVisit(object sender, EventArgs e)
        {
            applicationCore ap = new applicationCore(selectedDate);
            ap.addContractorVisitToXml(comboBox1.Text.ToString(),
               richTextBoxAddTodaysContractorDescription.Text);
            loadDailyContractors(selectedDate);
            panelDisplayAddContractorVisit.Visible = false;
            labelUpdateTodaysContractors.Text = "Contractor Visit Added";
        }

        private void loadAllContractors()
        {
            try
            {
                applicationCore ap = new applicationCore(fileNameDate);
                DataSet ds = new DataSet();


                ds.ReadXml(ap.path + @"\contractors\lists\contractors.xml");

                dataGridView4.DataSource = ds;
                if (ds.Tables.Count > 0)
                {
                    dataGridView4.DataMember = "contractor";
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                //error warning
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        private void loadAllProviders()
        {
            try
            {
                applicationCore ap = new applicationCore(fileNameDate);
                DataSet ds = new DataSet();


                ds.ReadXml(ap.path + @"\contractors\providers\lists\providers.xml");

                dataGridView8.DataSource = ds;
                if (ds.Tables.Count > 0)
                {
                    dataGridView8.DataMember = "provider";
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                //error warning
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public void loadAllTheTasks()
        {
            try
            {

                applicationCore ap = new applicationCore(fileNameDate);
                DataSet ds = new DataSet();

                ds.ReadXml(ap.path + @"tasks/tasklist.xml").ToString();

                dataGridView2.DataSource = ds;
                if (ds.Tables.Count > 0)
                {
                    dataGridView2.DataMember += "task";
                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        private void updateAddOneOffDailyTask(object sender, EventArgs e)
        {
            dataBase db = new dataBase();
            applicationCore ap = new applicationCore(fileNameDate);
            ArrayList element = new ArrayList();
            //add the xml elements names
            element.Add("date");
            element.Add("task");
            element.Add("description");
            element.Add("completed");
            ArrayList text = new ArrayList();
            //add the xml elements text values
            text.Add(fileNameDate);
            text.Add(textBoxTodaysTasksExtraTask.Text);
            text.Add(richTextBoxTodaysTaskAddDescrition.Text);
            if (checkBoxAddOneOffTask.Checked)
            {
                text.Add("true");
            }
            else
            {
                text.Add("false");
            }
            db.write(ap.path + selectedDate + @".xml", element, text, "tasks");
            labelAddOneOffTaskValidate.Text = "Task added to todays tasks";
            loadTodaysTasks(selectedDate);
        }

        private void addTask(object sender, EventArgs e)
        {
            try
            {
                if (textBoxFutureName.Text.Length > 3)
                {
                    dataBase db = new dataBase();
                    applicationCore ap = new applicationCore(fileNameDate);
                    ArrayList element = new ArrayList();
                    //add the xml elements names
                    element.Add("task_name");
                    element.Add("description");
                    element.Add("frequency");
                    ArrayList text = new ArrayList();

                    //add the xml elements text values
                    text.Add(textBoxFutureName.Text);
                    text.Add("");
                    if (radioButtonDailyTask.Checked) { text.Add("daily"); }
                    else if (radioButtonWeeklyTask.Checked) { text.Add("weekly"); }
                    else if (radioButtonMonthlyTask.Checked) { text.Add("monthly"); }
                    else if (radioButtonTwoMonthly.Checked)
                    {
                        text.Add("two_monthly");
                        element.Add("month");
                        text.Add(DateTime.Now.ToString("MMMM"));
                    }
                    else if (radioButtonThreeMonthly.Checked)
                    {
                        text.Add("three_monthly");
                        element.Add("month");
                        text.Add(DateTime.Now.ToString("MMMM"));
                    }
                    else if (radioButtonSixMonthly.Checked)
                    {
                        text.Add("six_monthly");
                        element.Add("month");
                        text.Add(DateTime.Now.ToString("MMMM"));
                    }

                    //write new task to tasklist
                    db.write(ap.path + @"tasks/tasklist.xml", element, text, "task");

                    loadAllTheTasks();
                    //add/ write the new task to todays task
                    if (radioButtonDailyTask.Checked)
                    {
                        ArrayList elementName = new ArrayList();
                        elementName.Add("task");
                        elementName.Add("description");
                        elementName.Add("completed");
                        ArrayList textValue = new ArrayList();
                        textValue.Add(textBoxFutureName.Text);
                        textValue.Add("");
                        textValue.Add("false");
                        db.write(ap.path + fileNameDate + ".xml", elementName, textValue, "tasks");
                    }
                    if (radioButtonWeeklyTask.Checked)
                    {
                        for (int w = 0; w < 4; w++)
                        {
                            ArrayList elementNameTwo = new ArrayList();
                            elementNameTwo.Add("date");
                            elementNameTwo.Add("task");
                            elementNameTwo.Add("description");
                            elementNameTwo.Add("weekly_or_monthly");
                            elementNameTwo.Add("completed");
                            elementNameTwo.Add("date_completed");
                            ArrayList textValueTwo = new ArrayList();
                            textValueTwo.Add(fileNameDate);
                            textValueTwo.Add(textBoxFutureName.Text);
                            textValueTwo.Add("");
                            textValueTwo.Add("weekly");

                            textValueTwo.Add("false");
                            textValueTwo.Add("");
                            db.write(ap.path + @"extra\" + DateTime.Now.ToString("MMMM-yyyy") + ".xml",
                                elementNameTwo, textValueTwo, "extra_tasks");
                        }
                    }
                    if (radioButtonMonthlyTask.Checked)
                    {
                        ArrayList elementNameTwo = new ArrayList();
                        elementNameTwo.Add("date");
                        elementNameTwo.Add("task");
                        elementNameTwo.Add("description");
                        elementNameTwo.Add("weekly_or_monthly");
                        elementNameTwo.Add("completed");
                        elementNameTwo.Add("date_completed");
                        ArrayList textValueTwo = new ArrayList();
                        textValueTwo.Add(fileNameDate);
                        textValueTwo.Add(textBoxFutureName.Text);
                        textValueTwo.Add("");

                        textValueTwo.Add("monthly");
                        textValueTwo.Add("false");
                        textValueTwo.Add("");
                        db.write(ap.path + @"extra\" + DateTime.Now.ToString("MMMM-yyyy") + ".xml",
                            elementNameTwo, textValueTwo, "extra_tasks");

                    }
                    loadTodaysTasks(selectedDate);
                    loadExtraTasks();

                    labelAddTaskValidate.Text = "Task added succesfully";
                }
                else { labelAddTaskValidate.Text = "Task must not already exist and has to be over three letters long."; }
            }

            catch (Exception err) { labelAddTaskValidate.Text = "Adding task failed";
            debugTerminal terminal = new debugTerminal();
            terminal.output(err.ToString());
            }
        }

        private void AddContractors(object sender, EventArgs e)
        {
            try
            {
                dataBase db = new dataBase();
                applicationCore ap = new applicationCore(fileNameDate);
                ArrayList element = new ArrayList();
                //add the xml elements names
                element.Add("name");
                element.Add("company");
                element.Add("phone");
                element.Add("description");
                element.Add("next_visit");
                element.Add("certification_no");
                ArrayList text = new ArrayList();
                text.Add(textBoxAddContractorName.Text);
                text.Add(textBoxAddContractorCompany.Text);
                text.Add(textBoxAddContractorPhone.Text);
                text.Add(richTextBoxAddContractorDescription.Text);
                text.Add(textBoxAddContractorNextVisit.Text);
                text.Add(textBoxAddContractorCertNumber.Text);

                //if contractor visits is re-occuring
                if (checkBoxAddContractorEveryXMonths.Checked)
                {
                    element.Add("every");
                    element.Add("frequency");
                    text.Add(textBoxAddContractorAmountOfTimeBetweenVisits.Text);
                    text.Add(comboBoxAddContractorPeriodOfTime.Text);
                }

                db.write(ap.path + @"\contractors\lists\contractors.xml",
                                element, text, "contractor");
                labelAddContractorValidate.Text = "Adding contractor succeded";
                loadAllContractors();
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString()); 
                labelAddContractorValidate.Text = "Adding contractor failed";
            }
        }

        private void AddProvider(object sender, EventArgs e)
        {
            try
            {
                dataBase db = new dataBase();
                applicationCore ap = new applicationCore(fileNameDate);
                ArrayList element = new ArrayList();
                //add the xml elements names
                element.Add("name");
                element.Add("due_date");
                element.Add("date_completed");
                ArrayList text = new ArrayList();
                text.Add(textBoxAddProvidersName.Text);
                text.Add(textBoxAddProviderDueDate.Text);
                text.Add(textBoxAddProviderDateCompleted.Text);

                db.write(ap.path + @"\contractors\providers\lists\providers.xml",
                                element, text, "provider");
                labelAddProviderValidate.Text = "Adding provider succeded";
                loadAllProviders();
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString()); 
                labelAddProviderValidate.Text = "Adding provider failed";
            }
        }

        private void addItemMaintenenceItem(object sender, EventArgs e)
        {
            try
            {
                dataBase db = new dataBase();
                applicationCore ap = new applicationCore(fileNameDate);
                ArrayList element = new ArrayList();
                //add the xml elements names
                element.Add("area");
                element.Add("room");
                element.Add("item_name");
                element.Add("description");
                element.Add("next_visit");
                ArrayList text = new ArrayList();
                text.Add(textBoxItemMaintenenceArea.Text);
                text.Add(textBoxAddItemMaintenceRoom.Text);
                text.Add(textBoxAddItemMaintenenceItem.Text);
                text.Add(richTextBoxAddItemMaintenceDescription.Text);
                text.Add(textBoxAddContractorNextVisit.Text);
                db.write(ap.path + @"maintenence\" + selectedDate + ".xml",
                                element, text, "item");
                loadAllMaintenceItems(selectedDate);
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        private void updateMaintenenceItems(object sender, EventArgs e)
        {
            applicationCore ap = new applicationCore(fileNameDate);
            datasetToXml(ap.path + @"maintenence\" + selectedDate + ".xml", 6);
        }

        private void updateProviders(object sender, EventArgs e)
        {
            applicationCore ap = new applicationCore(fileNameDate);
            datasetToXml(ap.path + @"contractors\providers\lists\providers.xml", 8);
        }

        private void loadAllMaintenceItems(string date)
        {
            try
            {
                applicationCore ap = new applicationCore(fileNameDate);
                DataSet ds = new DataSet();


                ds.ReadXml(ap.path + @"\maintenence\" + date + ".xml");

                dataGridView6.DataSource = ds;
                if (ds.Tables.Count > 0)
                {
                    dataGridView6.DataMember = "item";
                }

            }
            catch (System.IO.FileNotFoundException)
            {
                //error warning
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }

        }

        private void datasetToXml(string fileName, int dgv)
        {
            try
            {
                DataSet ds = new DataSet();
                switch (dgv)
                {
                    case 1:
                        ds = (DataSet)dataGridView1.DataSource;
                        for (int i = 0; i < ds.Tables.Count; i++)
                        {
                            ds.Tables[i].WriteXml(fileName, System.Data.XmlWriteMode.IgnoreSchema);
                            loadTodaysTasks(selectedDate);
                            labelTodaysTasksUpdate.Text = "Tasks Succesfully Updated";
                        }
                        break;
                    case 2:

                        ds = (DataSet)dataGridView2.DataSource;
                        for (int i = 0; i < ds.Tables.Count; i++)
                        {

                            ds.Tables[i].WriteXml(fileName, System.Data.XmlWriteMode.IgnoreSchema);
                            loadTodaysTasks(selectedDate);
                            labelAddTasksUpdate.Text = "Tasks Succesfully Updated";
                        }
                        break;
                    case 3:
                        ds = (DataSet)dataGridView3.DataSource;
                        for (int i = 0; i < ds.Tables.Count; i++)
                        {
                            ds.Tables[i].WriteXml(fileName, System.Data.XmlWriteMode.IgnoreSchema);
                            loadExtraTasks();
                            labelExtraTasksUpdate.Text = "Tasks Succesfully Updated";
                        }
                        break;
                    case 4:
                        ds = (DataSet)dataGridView4.DataSource;
                        for (int i = 0; i < ds.Tables.Count; i++)
                        {
                            ds.Tables[i].WriteXml(fileName, System.Data.XmlWriteMode.IgnoreSchema);
                            loadAllContractors();
                            labelUpdateContractors.Text = "Contractors Succesfully Updated";
                        }
                        break;
                    case 5:
                        ds = (DataSet)dataGridView5.DataSource;
                        for (int i = 0; i < ds.Tables.Count; i++)
                        {
                            ds.Tables[i].WriteXml(fileName, System.Data.XmlWriteMode.IgnoreSchema);
                            loadAllContractors();
                            labelUpdateTodaysContractors.Text = "Contractors Succesfully Updated";
                        }
                        break;
                    case 6:
                        ds = (DataSet)dataGridView6.DataSource;
                        for (int i = 0; i < ds.Tables.Count; i++)
                        {
                            ds.Tables[i].WriteXml(fileName, System.Data.XmlWriteMode.IgnoreSchema);
                            loadAllMaintenceItems(selectedDate);

                        }
                        break;
                    case 7:
                        ds = (DataSet)dataGridView7.DataSource;
                        for (int i = 0; i < ds.Tables.Count; i++)
                        {
                            ds.Tables[i].WriteXml(fileName, System.Data.XmlWriteMode.IgnoreSchema);
                            loadInventoryItems();

                        }
                        break;
                    case 8:
                        ds = (DataSet)dataGridView8.DataSource;
                        for (int i = 0; i < ds.Tables.Count; i++)
                        {
                            ds.Tables[i].WriteXml(fileName, System.Data.XmlWriteMode.IgnoreSchema);
                            loadAllProviders();

                        }
                        break;
                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        private void updateTodaysTasks(object sender, EventArgs e)
        {
            applicationCore ap = new applicationCore(fileNameDate);
            datasetToXml(ap.path + selectedDate + ".xml", 1);

        }

        private void updateAddTasks(object sender, EventArgs e)
        {
            applicationCore ap = new applicationCore(fileNameDate);
            datasetToXml(ap.path + @"tasks/tasklist.xml", 2);
        }

        private void updateExtraTasks(object sender, EventArgs e)
        {
            applicationCore ap = new applicationCore(fileNameDate);
            datasetToXml(ap.path + @"extra\" + DateTime.Now.ToString("MMMM-yyyy") + ".xml", 3);
        }


        private void updateContractors(object sender, EventArgs e)
        {
            applicationCore ap = new applicationCore(fileNameDate);
            datasetToXml(ap.path + @"\contractors\lists\contractors.xml", 4);

        }

        private void updateTodaysContractors(object sender, EventArgs e)
        {
            applicationCore ap = new applicationCore(fileNameDate);
            datasetToXml(ap.path + @"\contractors\" + selectedDate + ".xml", 5);
        }

        private void removeDeletedRowFromOtherTasks(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                dataBase db = new dataBase();
                applicationCore ap = new applicationCore(fileNameDate);
                //get the name of the task from the deleted row and the first cell
                string deletedTaskValue = (string)dataGridView2.Rows[e.Row.Index].Cells[1].Value;
                string deletedFreqValue = (string)dataGridView2.Rows[e.Row.Index].Cells[2].Value;
                //delete rows with the same task name from other xml files
                if (deletedFreqValue == "daily")
                {
                    db.delete(ap.path + fileNameDate + ".xml", "/root/tasks[task='" + deletedTaskValue + "']");
                    loadTodaysTasks(selectedDate);
                }
                if (deletedFreqValue == "weekly" || deletedFreqValue == "monthly")
                    for (int w = 0; w < 4; w++)
                    {
                        db.delete(ap.path + @"extra\" + DateTime.Now.ToString("MMMM-yyyy") + ".xml", "/extra_tasks/extra_tasks[task='" + deletedTaskValue + "']");
                    }
                loadExtraTasks();
                //now we update the table
                datasetToXml(ap.path + @"tasks/tasklist.xml", 2);
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        private void clickExportTasksToExcel(object sender, EventArgs e)
        {

            if (comboBoxChooseSaveList.Text == "Tasks")
            {
                progressBar1.Visible = true;
                progressBar1.Value = 25;
                progressBar1.Update();
                exportTasksToCSV();
            }
            else if (comboBoxChooseSaveList.Text == "Contractors")
            {
                progressBar1.Visible = true;
                progressBar1.Value = 25;
                progressBar1.Update();
                exportContractorsToCSV();
            }
            else if (comboBoxChooseSaveList.Text == "Contractor Visits")
            {
                progressBar1.Visible = true;
                progressBar1.Value = 25;
                progressBar1.Update();
                exportContractorVisitsToCSV();
            }
            else if (comboBoxChooseSaveList.Text == "Providers")
            {
                progressBar1.Visible = true;
                progressBar1.Value = 25;
                progressBar1.Update();
                exportProvidersToCSV();
            }
            else if (comboBoxChooseSaveList.Text == "Maintenence")
            {
                progressBar1.Visible = true;
                progressBar1.Value = 25;
                progressBar1.Update();
                exportMaintenenceToCSV();
            }
            else if (comboBoxChooseSaveList.Text == "Inventory")
            {
                progressBar1.Visible = true;
                progressBar1.Value = 25;
                progressBar1.Update();
                exportInventoryToCSV();
            }
            else
            {//do nothing}
                MessageBox.Show("You must select an list to save");
            }
            if (File.Exists(Application.StartupPath + @"\database\temp\schema.ini"))
            {
                File.Delete(Application.StartupPath + @"\database\temp\schema.ini");
            }
        }

        private void exportTasksToCSV()
        {
            try
            {
                //first step is too check if temp file exists and if it does delete it
                //to start again
                if (File.Exists(Application.StartupPath + @"\database\temp\data.csv"))
                {
                    File.Delete(Application.StartupPath + @"\database\temp\data.csv");
                }
                //now we get the value of all input and selection fields and convert them to appropriate data type
                int sd = Convert.ToInt32(textBoxSaveToCSVStartDate.Text);
                int fd = Convert.ToInt32(textBoxSaveToCSVEndDay.Text);
                if (sd > 0 && sd < 31 && fd > 0 && fd < 31)
                {
                    string sm = comboBoxSaveToCSVStartMonth.Text;
                    string fm = comboBoxSaveToCSVEndMonth.Text;

                    string sy = textBoxSaveToCSVStartYear.Text;
                    string fy = textBoxSaveToCSVEndYear.Text;
                    applicationCore apc = new applicationCore(fileNameDate);
                    exportToCSV CSV = new exportToCSV();

                    //get selected months as numbers
                    int startMonth = apc.getMonthAsInt(sm);
                    int endMonth = apc.getMonthAsInt(fm);

                    int nsy = Convert.ToInt32(sy);
                    int nfy = Convert.ToInt32(fy);

                    //now we get data from each file between te selected time period
                    //based on its date based file name

                    CSV.createTasksCSVDocument();

                    //loop through years
                    for (int y = nsy; y <= nfy; y++)
                    {
                        //loop through months
                        for (int m = startMonth; m <= endMonth; m++)
                        {
                            string thisMonthsName = apc.getMonthAsString(m);

                            if (File.Exists(Application.StartupPath + @"\database\" + y + @"\extra\" + thisMonthsName + "-" + y + ".xml"))
                            {
                                CSV.generateTasksCSVDocument(Application.StartupPath + @"\database\" + y + @"\extra\" + thisMonthsName + "-" + y + ".xml",
                                DateTime.Now.ToString("d/MM/yyyy"), "extra_tasks");
                            }
                            //loop through days
                            for (int d = sd; d <= 31; d++)
                            {
                                try
                                {
                                    if (File.Exists(Application.StartupPath + @"\database\" + y + @"\" + d + "-" + m + "-" + y + ".xml"))
                                    {
                                        CSV.generateTasksCSVDocument(Application.StartupPath + @"\database\" + y + @"\" + d + "-" + m + "-" + y + ".xml",
                                        DateTime.Now.ToString("d/MM/yyyy"), "tasks");
                                    }

                                }
                                catch (Exception err)
                                {
                                    debugTerminal terminal = new debugTerminal();
                                    terminal.output(err.ToString());
                                }
                            }
                        }
                    }
                    //now we let the user save the temp excel file
                    SaveFileDialog save = new SaveFileDialog();
                    save.Filter = "Excel|*.CSV";
                    save.FileName = "myTaskCheckList.csv";
                    progressBar1.Value = 50;

                    save.ShowDialog();
                    File.Copy(Application.StartupPath + @"\database\temp\data.csv", save.FileName);
                    //exportCSVValidate.Text = "File Saved";
                    //delete temp file
                    File.Delete(Application.StartupPath + @"\database\temp\data.csv");
                    progressBar1.Value = 100;
                }

                else
                {
                    exportCSVValidate.Text = "The days added were not correct month numbers";
                }
            }
            catch (Exception err)
            {
                exportCSVValidate.Text = "File was not saved";
                progressBar1.Value = 0;
                if (File.Exists(Application.StartupPath + @"\database\temp\data.csv"))
                {
                    File.Delete(Application.StartupPath + @"\database\temp\data.csv");
                    debugTerminal terminal = new debugTerminal();
                    terminal.output(err.ToString());
                }
            }
        }

        private void exportContractorsToCSV()
        {
            try
            {
                //first step is too check if temp file exists and if it does delete it
                //to start again
                if (File.Exists(Application.StartupPath + @"\database\temp\contractorlists.csv"))
                {
                    File.Delete(Application.StartupPath + @"\database\temp\contractorlists.csv");
                }
                //now we get the value of all input and selection fields and convert them to appropriate data type
                exportToCSV CSV = new exportToCSV();
                CSV.createContractorListsCSVDocument();
                string year = textBoxSaveToCSVEndYear.Text;

                try
                {
                    if (File.Exists(Application.StartupPath + @"\database\" + year + @"\contractors\lists\contractors.xml"))
                    {

                        CSV.generateContractorListsCSVDocument(Application.StartupPath + @"\database\" + year + @"\contractors\lists\contractors.xml",
                        DateTime.Now.ToString("d/MM/yyyy"), "contractor");
                    }

                }
                catch (Exception err)
                {
                    debugTerminal terminal = new debugTerminal();
                    terminal.output(err.ToString());
                }



                //now we let the user save the temp excel file
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "Excel|*.CSV";
                save.FileName = "myContractorList.csv";
                progressBar1.Value = 50;

                save.ShowDialog();
                File.Copy(Application.StartupPath + @"\database\temp\contractorlists.csv", save.FileName);
                //exportCSVValidate.Text = "File Saved";
                //delete temp file
                File.Delete(Application.StartupPath + @"\database\temp\contractorlists.csv");
                progressBar1.Value = 100;



            }
            catch (Exception err)
            {

                exportCSVValidate.Text = "File was not saved";
                progressBar1.Value = 0;
                if (File.Exists(Application.StartupPath + @"\database\temp\contractorlists.csv"))
                {
                    File.Delete(Application.StartupPath + @"\database\temp\contractorlists.csv");
                    debugTerminal terminal = new debugTerminal();
                    terminal.output(err.ToString());
                }

            }
        }

        private void exportProvidersToCSV()
        {
            try
            {
                //first step is too check if temp file exists and if it does delete it
                //to start again
                if (File.Exists(Application.StartupPath + @"\database\temp\providers.csv"))
                {
                    File.Delete(Application.StartupPath + @"\database\temp\providers.csv");
                }
                //now we get the value of all input and selection fields and convert them to appropriate data type
                exportToCSV CSV = new exportToCSV();
                CSV.createProvidersCSVDocument();
                string year = textBoxSaveToCSVEndYear.Text;

                try
                {
                    if (File.Exists(Application.StartupPath + @"\database\" + year + @"\contractors\providers\lists\providers.xml"))
                    {

                        CSV.generateProvidersCSVDocument(Application.StartupPath + @"\database\" + year + @"\contractors\providers\lists\providers.xml",
                        DateTime.Now.ToString("d/MM/yyyy"), "provider");
                    }

                }
                catch (Exception err)
                {
                    debugTerminal terminal = new debugTerminal();
                    terminal.output(err.ToString()); 
                }



                //now we let the user save the temp excel file
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "Excel|*.CSV";
                save.FileName = "myProvidersList.csv";
                progressBar1.Value = 50;

                save.ShowDialog();
                File.Copy(Application.StartupPath + @"\database\temp\providers.csv", save.FileName);
                //exportCSVValidate.Text = "File Saved";
                //delete temp file
                File.Delete(Application.StartupPath + @"\database\temp\providers.csv");
                progressBar1.Value = 100;



            }
            catch (Exception err)
            {

                exportCSVValidate.Text = "File was not saved";
                progressBar1.Value = 0;
                if (File.Exists(Application.StartupPath + @"\database\temp\providers.csv"))
                {
                    File.Delete(Application.StartupPath + @"\database\temp\providers.csv");
                    debugTerminal terminal = new debugTerminal();
                    terminal.output(err.ToString());
                }

            }
        }

        private void exportInventoryToCSV()
        {
            try
            {
                //first step is too check if temp file exists and if it does delete it
                //to start again
                if (File.Exists(Application.StartupPath + @"\database\temp\inventory.csv"))
                {
                    File.Delete(Application.StartupPath + @"\database\temp\inventory.csv");
                }
                //now we get the value of all input and selection fields and convert them to appropriate data type
                exportToCSV CSV = new exportToCSV();
                CSV.createInventoryCSVDocument();

                string year = textBoxSaveToCSVEndYear.Text;
                try
                {
                    if (File.Exists(Application.StartupPath + @"\database\" + year + @"\maintenence\inventory\list.xml"))
                    {

                        CSV.generateInventoryCSVDocument(Application.StartupPath + @"\database\" + year + @"\maintenence\inventory\list.xml",
                        DateTime.Now.ToString("d/MM/yyyy"), "items");
                    }

                }
                catch (Exception err)
                {
                    debugTerminal terminal = new debugTerminal();
                    terminal.output(err.ToString());
                }



                //now we let the user save the temp excel file
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "Excel|*.CSV";
                save.FileName = "myInventory.csv";
                progressBar1.Value = 50;

                save.ShowDialog();
                File.Copy(Application.StartupPath + @"\database\temp\inventory.csv", save.FileName);
                //exportCSVValidate.Text = "File Saved";
                //delete temp file
                File.Delete(Application.StartupPath + @"\database\temp\inventory.csv");
                progressBar1.Value = 100;



            }
            catch (Exception err)
            {

                exportCSVValidate.Text = "File was not saved";
                progressBar1.Value = 0;
                if (File.Exists(Application.StartupPath + @"\database\temp\inventory.csv"))
                {
                    File.Delete(Application.StartupPath + @"\database\temp\inventory.csv");
                    debugTerminal terminal = new debugTerminal();
                    terminal.output(err.ToString());
                }
            }
        }

        private void exportContractorVisitsToCSV()
        {
            try
            {
                //first step is too check if temp file exists and if it does delete it
                //to start again
                if (File.Exists(Application.StartupPath + @"\database\temp\contractors.csv"))
                {
                    File.Delete(Application.StartupPath + @"\database\temp\contractors.csv");
                }
                //now we get the value of all input and selection fields and convert them to appropriate data type
                int sd = Convert.ToInt32(textBoxSaveToCSVStartDate.Text);
                int fd = Convert.ToInt32(textBoxSaveToCSVEndDay.Text);
                if (sd > 0 && sd < 31 && fd > 0 && fd < 31)
                {
                    string sm = comboBoxSaveToCSVStartMonth.Text;
                    string fm = comboBoxSaveToCSVEndMonth.Text;

                    string sy = textBoxSaveToCSVStartYear.Text;
                    string fy = textBoxSaveToCSVEndYear.Text;
                    applicationCore apc = new applicationCore(fileNameDate);
                    exportToCSV CSV = new exportToCSV();

                    //get selected months as numbers
                    int startMonth = apc.getMonthAsInt(sm);
                    int endMonth = apc.getMonthAsInt(fm);

                    int nsy = Convert.ToInt32(sy);
                    int nfy = Convert.ToInt32(fy);

                    //now we get data from each file between te selected time period
                    //based on its date based file name

                    CSV.createContractorsCSVDocument();

                    //loop through years
                    for (int y = nsy; y <= nfy; y++)
                    {
                        //loop through months
                        for (int m = startMonth; m <= endMonth; m++)
                        {

                            //loop through days
                            for (int d = sd; d <= 31; d++)
                            {
                                try
                                {
                                    if (File.Exists(Application.StartupPath + @"\database\" + y + @"\contractors\" + d + "-" + m + "-" + y + ".xml"))
                                    {

                                        CSV.generateContractorsCSVDocument(Application.StartupPath + @"\database\" + y + @"\contractors\" + d + "-" + m + "-" + y + ".xml",
                                        DateTime.Now.ToString("d/MM/yyyy"), "visitor");
                                    }

                                }
                                catch (Exception err)
                                {
                                    debugTerminal terminal = new debugTerminal();
                                    terminal.output(err.ToString());
                                }
                            }
                        }
                    }
                    //now we let the user save the temp excel file
                    SaveFileDialog save = new SaveFileDialog();
                    save.Filter = "Excel|*.CSV";
                    save.FileName = "myContractorCheckList.csv";
                    progressBar1.Value = 50;

                    save.ShowDialog();
                    File.Copy(Application.StartupPath + @"\database\temp\contractors.csv", save.FileName);
                    //exportCSVValidate.Text = "File Saved";
                    //delete temp file
                    File.Delete(Application.StartupPath + @"\database\temp\contractors.csv");
                    progressBar1.Value = 100;
                }

                else
                {
                    exportCSVValidate.Text = "The days added were not correct month numbers";
                }
            }
            catch (Exception err)
            {

                exportCSVValidate.Text = "File was not saved";
                progressBar1.Value = 0;
                if (File.Exists(Application.StartupPath + @"\database\temp\contractors.csv"))
                {
                    File.Delete(Application.StartupPath + @"\database\temp\contractors.csv");
                    debugTerminal terminal = new debugTerminal();
                    terminal.output(err.ToString());
                }
            }
        }

        private void exportMaintenenceToCSV()
        {
            try
            {
                //first step is too check if temp file exists and if it does delete it
                //to start again
                if (File.Exists(Application.StartupPath + @"\database\temp\maintenence.csv"))
                {
                    File.Delete(Application.StartupPath + @"\database\temp\maintenence.csv");
                }
                //now we get the value of all input and selection fields and convert them to appropriate data type
                int sd = Convert.ToInt32(textBoxSaveToCSVStartDate.Text);
                int fd = Convert.ToInt32(textBoxSaveToCSVEndDay.Text);
                if (sd > 0 && sd < 31 && fd > 0 && fd < 31)
                {
                    string sm = comboBoxSaveToCSVStartMonth.Text;
                    string fm = comboBoxSaveToCSVEndMonth.Text;

                    string sy = textBoxSaveToCSVStartYear.Text;
                    string fy = textBoxSaveToCSVEndYear.Text;
                    applicationCore apc = new applicationCore(fileNameDate);
                    exportToCSV CSV = new exportToCSV();

                    //get selected months as numbers
                    int startMonth = apc.getMonthAsInt(sm);
                    int endMonth = apc.getMonthAsInt(fm);

                    int nsy = Convert.ToInt32(sy);
                    int nfy = Convert.ToInt32(fy);

                    //now we get data from each file between te selected time period
                    //based on its date based file name

                    CSV.createMaintenenceCSVDocument();

                    //loop through years
                    for (int y = nsy; y <= nfy; y++)
                    {
                        //loop through months
                        for (int m = startMonth; m <= endMonth; m++)
                        {

                            //loop through days
                            for (int d = sd; d <= 31; d++)
                            {
                                try
                                {
                                    if (File.Exists(Application.StartupPath + @"\database\" + y + @"\maintenence\" + d + "-" + m + "-" + y + ".xml"))
                                    {

                                        CSV.generateMaintenenceCSVDocument(Application.StartupPath + @"\database\" + y + @"\maintenence\" + d + "-" + m + "-" + y + ".xml",
                                        DateTime.Now.ToString("d/MM/yyyy"), "item");
                                    }

                                }
                                catch (Exception err)
                                {
                                    debugTerminal terminal = new debugTerminal();
                                    terminal.output(err.ToString());
                                }
                            }
                        }
                    }
                    //now we let the user save the temp excel file
                    SaveFileDialog save = new SaveFileDialog();
                    save.Filter = "Excel|*.CSV";
                    save.FileName = "myMaintenenceCheckList.csv";
                    progressBar1.Value = 50;

                    save.ShowDialog();
                    File.Copy(Application.StartupPath + @"\database\temp\maintenence.csv", save.FileName);
                    //exportCSVValidate.Text = "File Saved";
                    //delete temp file
                    File.Delete(Application.StartupPath + @"\database\temp\maintenence.csv");
                    progressBar1.Value = 100;
                }

                else
                {
                    exportCSVValidate.Text = "The days added were not correct month numbers";
                }
            }
            catch (Exception err)
            {

                exportCSVValidate.Text = "File was not saved";
                progressBar1.Value = 0;
                if (File.Exists(Application.StartupPath + @"\database\temp\maintenence.csv"))
                {
                    File.Delete(Application.StartupPath + @"\database\temp\maintenence.csv");
                    debugTerminal terminal = new debugTerminal();
                    terminal.output(err.ToString());
                }
            }
        }

        private void menuQuitProgram(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you would like to quit?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void leftNavTodaysTasks(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPageTaskList;
            TasksSelection.SelectedTab = tabPageChooseTodaysTasks;
        }

        private void leftNavWeeklyTasks(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPageTaskList;
            TasksSelection.SelectedTab = tabPageChooseWeeklyTasks;
        }

        private void leftNavAddTasks(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPageTaskList;
            TasksSelection.SelectedTab = tabPageChooseAddTasks;
        }

        private void leftNavSaveTaskLists(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPageExportLists;
            SaveSelection.SelectedTab = tabPageSaveToExcel;
        }

        private void leftNavContractorVisit(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPageContractors;
            tabControl2.SelectedTab = tabPageThisMonthsContractors;
        }

        private void leftNavLegislative(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPageMaintenanceLegislation;
        }

        private void leftNavManageContractors(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPageContractors;
            tabControl2.SelectedTab = tabPageAddContractors;
        }

        private void leftNavInventoryMaintenence(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPageInventoryMaintenence;
            tabControl3.SelectedTab = tabPageItemMaintenence;
        }

        private void leftNavInventoryStock(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPageInventoryMaintenence;
            tabControl3.SelectedTab = tabPageInventory;
        }

        private void launchDegugTerminal(object sender, EventArgs e)
        {
            debugTerminal terminal = new debugTerminal();
            terminal.Visible = true;

        }

        public void loadDashboard() {
            //if todays tasklist has not yet been generated do it now
            applicationCore ap = new applicationCore(fileNameDate);
            dataBase db = new dataBase();
            richTextBoxDashTasks.Text += "Daily Tasks that need attention \n\n";
            XmlDocument xdoc = new XmlDocument();
            FileStream rfile = new FileStream(ap.path + fileNameDate + ".xml", FileMode.Open);
            xdoc.Load(rfile);
            XmlNodeList list = xdoc.GetElementsByTagName("task");
            for (int i = 0; i < list.Count; i++)
            {
                XmlElement taskName = (XmlElement)xdoc.GetElementsByTagName("task")[i];
                XmlElement completed = (XmlElement)xdoc.GetElementsByTagName("completed")[i];
                if (completed.InnerText == "false")
                {
                    richTextBoxDashTasks.Text += taskName.InnerText + " - Not Completed\n" ;
                }
            }

            richTextBoxDashTasks.Text += "\n\nWeekly & Monthly Tasks that need attention \n\n";
            //now extra tasks
            rfile = new FileStream(ap.path + @"extra\" + DateTime.Now.ToString("MMMM-yyyy") + ".xml", FileMode.Open);
            xdoc.Load(rfile);
            list = xdoc.GetElementsByTagName("task");
            for (int i = 0; i < list.Count; i++)
            {
                XmlElement taskName = (XmlElement)xdoc.GetElementsByTagName("task")[i];
                XmlElement completed = (XmlElement)xdoc.GetElementsByTagName("completed")[i];
                XmlElement frequency = (XmlElement)xdoc.GetElementsByTagName("weekly_or_monthly")[i];
                if (completed.InnerText == "false")
                {
                    richTextBoxDashTasks.Text += taskName.InnerText + " - Not Completed\n";
                }
            }
            rfile.Close(); 
            //contractors
            richTextBoxDashTasks.Text += "\n\nContractors Visting \n\n";
            //now extra tasks
            rfile = new FileStream(ap.path + @"contractors\lists\contractors.xml", FileMode.Open);
            xdoc.Load(rfile);
            list = xdoc.GetElementsByTagName("contractor");
            for (int i = 0; i < list.Count; i++)
            {
                XmlElement name = (XmlElement)xdoc.GetElementsByTagName("name")[i];
                XmlElement company = (XmlElement)xdoc.GetElementsByTagName("company")[i];
                XmlElement next = (XmlElement)xdoc.GetElementsByTagName("next_visit")[i];
                int result = DateTime.Compare(Convert.ToDateTime(next.InnerText), DateTime.Now);
                if (result >= 0)
                {
                    richTextBoxDashTasks.Text += name.InnerText + " from " + company.InnerText + " - Due in "+result+" days \n";
                }
            }
            rfile.Close(); 
            
        }
    }
}

       
