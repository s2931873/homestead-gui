﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Net;
using System.Diagnostics;


namespace MaintenenceGUI 
{
    class applicationCore 
    {
        public applicationCore(string todaysDate)
        {

             
        }

        private static string fileNameDate;
        public string path = Application.StartupPath + @"\database\root\";
       
        public void startUp()
        {
            try
            {
                dataBase db = new dataBase();
                fileFolder ff = new fileFolder();
                //check the core folder database exists - if not create it
                ff.createFolder(path);
                ff.createFolder(path + @"extra\");
                ff.createFolder(path + @"tasks\");
                ff.createFolder(path + @"maintenence\");
                ff.createFolder(path + @"maintenence\inventory\");
                ff.createFolder(path + @"contractors\");
                ff.createFolder(path + @"contractors\lists\");
                ff.createFolder(path + @"\contractors\providers\lists\");
                ff.createFolder(Application.StartupPath + @"\database\config\");
                ff.createFolder(Application.StartupPath + @"\database\temp\");
                
                db.create("contractor_lists", path + @"\contractors\lists\contractors.xml");
                db.create("contractors_today", path + @"\contractors\" + fileNameDate + ".xml");
                db.create("provider_lists", path + @"\contractors\providers\lists\providers.xml");
               
       
                //check if tasks xml datbase exists
                db.create("tasks", path + @"tasks\tasklist.xml");
                if (db.create("extra_tasks", path + @"extra\" + DateTime.Now.ToString("MMMM-yyyy") + ".xml"))
                {
                    createExtraTasks();
                }
                

                //check if todays tasks xml datbase exists
                if (db.create("root", path + DateTime.Now.ToString("d-MM-yyyy") + ".xml"))
                {
                    //file does not exist returns true - create todays tasks
                    createTodaysTasks();
                }
                db.create("inventory", path + @"maintenence\inventory\list.xml");
                db.create("item_maintenence", path + @"maintenence\"+fileNameDate+".xml");
               
                //check for update
                
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public void createTodaysTasks() {
        //if todays tasklist has not yet been generated do it now
            dataBase db = new dataBase();
            
            XmlDocument xdoc = new XmlDocument(); 
            FileStream rfile = new FileStream(path + @"tasks\tasklist.xml", FileMode.Open);
            xdoc.Load(rfile);
            XmlNodeList list = xdoc.GetElementsByTagName("task");
            for (int i = 0; i < list.Count; i++)
            {
                XmlElement taskName = (XmlElement)xdoc.GetElementsByTagName("task_name")[i];
                XmlElement frequency = (XmlElement)xdoc.GetElementsByTagName("frequency")[i];
                if (frequency.InnerText == "daily")
                {
                    ArrayList element = new ArrayList();
                    ArrayList text = new ArrayList();
                    //the name of the xml element to be added
                    element.Add("date");
                    //the text inside the xml element to be added
                    text.Add(fileNameDate);
                    //the name of the xml element to be added
                    element.Add("task");
                    //the text inside the xml element to be added
                    text.Add(taskName.InnerText);
                    //the description text of the xml element to be added
                    element.Add("description");
                    //the xml text to be added
                    text.Add("");
                    //the description text of the xml element to be added
                    element.Add("completed");
                    //the text inside the xml element to be added
                    text.Add("false");
                    //save each task as an xml element
                    db.write(path + fileNameDate + ".xml", element, text, "tasks");
                }
            } 
            rfile.Close(); 
        }

        public void addContractorVisitToXml(string company, string description) {
            dataBase db = new dataBase();

            XmlDocument xdoc = new XmlDocument();
            FileStream rfile = new FileStream(path + @"contractors\lists\contractors.xml", FileMode.Open);
            xdoc.Load(rfile);
            XmlNodeList list = xdoc.GetElementsByTagName("contractor");
            for (int i = 0; i < list.Count; i++)
            {
                XmlElement xCompany = (XmlElement)xdoc.GetElementsByTagName("company")[i];
                if (xCompany.InnerText == company)
                {
                    XmlElement xName = (XmlElement)xdoc.GetElementsByTagName("name")[i];
                    ArrayList element = new ArrayList();
                    ArrayList text = new ArrayList();
                    //the name of the xml element to be added
                    element.Add("name");
                    //the text inside the xml element to be added
                    text.Add(xName.InnerText);
                    //the name of the xml element to be added
                    element.Add("company");
                    //the text inside the xml element to be added
                    text.Add(xCompany.InnerText);
                    //the description text of the xml element to be added
                    element.Add("description");
                    //the xml text to be added
                    text.Add(description);
                    //the description text of the xml element to be added
                    element.Add("date");
                    //the text inside the xml element to be added
                    text.Add(fileNameDate);
                    //save each task as an xml element
                    db.write(path + @"contractors\"+fileNameDate+".xml", element, text, "visitor");
                }
            }
            rfile.Close(); 
        }

        public string getContractorName(string company)
        {
            dataBase db = new dataBase();

            XmlDocument xdoc = new XmlDocument();
           
            xdoc.Load(path + @"contractors\lists\contractors.xml");
            XmlNodeList list = xdoc.GetElementsByTagName("contractor");
            for (int i = 0; i < list.Count; i++)
            {
                XmlElement xCompany = (XmlElement)xdoc.GetElementsByTagName("company")[i];
                
                if (xCompany.InnerText == company)
                {
                    XmlElement xName = (XmlElement)xdoc.GetElementsByTagName("name")[i];
                    return xName.InnerText;
                }
            }
           
            return "";
        }

        private void createExtraTasks()
        {
            
            try
            {

                //if extra tasklist has not yet been generated do it now
                dataBase db = new dataBase();

                XmlDocument xdoc = new XmlDocument();
                FileStream rfile = new FileStream(path + @"tasks\tasklist.xml", FileMode.Open);
                xdoc.Load(rfile);

                XmlNodeList list = xdoc.GetElementsByTagName("task");
                for (int i = 0; i <= list.Count; i++)
                {
                    XmlElement taskName = (XmlElement)xdoc.GetElementsByTagName("task_name")[i];
                    XmlElement frequency = (XmlElement)xdoc.GetElementsByTagName("frequency")[i];



                    if (frequency.InnerText == "weekly")
                    {
                        for (int w = 0; w < 4; w++)
                        {
                            ArrayList element = new ArrayList();
                            ArrayList text = new ArrayList();
                            //the name of the xml element to be added
                            element.Add("task");
                            //the text inside the xml element to be added
                            text.Add(taskName.InnerText);
                            //the name of the xml element to be added
                            element.Add("weekly_or_monthly");
                            //the text inside the xml element to be added
                            text.Add("weekly");
                            //the name of the xml element to be added
                            element.Add("completed");
                            //the text inside the xml element to be added
                            text.Add("false");
                            //the name of the xml element to be added
                            element.Add("date_completed");
                            //the text inside the xml element to be added
                            text.Add("");
                            //save each task as an xml element
                            db.write(path + @"extra\" + DateTime.Now.ToString("MMMM-yyyy") + ".xml", element, text, "extra_tasks");
                        }
                    }
                    else if (frequency.InnerText == "two_monthly")
                    {
                        string date = xdoc.GetElementsByTagName("month")[i].InnerText;
                        
                        DateTime startDate = DateTime.Now;

                            if (date == startDate.AddMonths(2).ToString("MMMM")
                             || date == startDate.AddMonths(4).ToString("MMMM")
                             || date == startDate.AddMonths(6).ToString("MMMM")
                             || date == startDate.AddMonths(8).ToString("MMMM")
                             || date == startDate.AddMonths(10).ToString("MMMM")
                             || date == startDate.AddMonths(12).ToString("MMMM"))
                            {
                                
                                ArrayList element = new ArrayList();
                                ArrayList text = new ArrayList();
                                //the name of the xml element to be added
                                element.Add("task");
                                //the text inside the xml element to be added
                                text.Add(taskName.InnerText);
                                //the name of the xml element to be added
                                element.Add("weekly_or_monthly");
                                //the text inside the xml element to be added
                                text.Add(frequency.InnerText);
                                //the name of the xml element to be added
                                element.Add("completed");
                                //the text inside the xml element to be added
                                text.Add("false");
                                //the name of the xml element to be added
                                element.Add("date_completed");
                                //the text inside the xml element to be added
                                text.Add("");
                                //save each task as an xml element
                                db.write(path + @"extra\" + DateTime.Now.ToString("MMMM-yyyy") + ".xml", element, text, "extra_tasks");
                            }

                            else {  }
                        
                    }

                    else if (frequency.InnerText == "three_monthly")
                    {
                        string date = xdoc.GetElementsByTagName("date")[i].ToString();
                        if (getMonthAsInt(date) == getMonthAsInt(date) || getMonthAsInt(date) == getMonthAsInt(date) + 3)
                        {
                            ArrayList element = new ArrayList();
                            ArrayList text = new ArrayList();
                            //the name of the xml element to be added
                            element.Add("task");
                            //the text inside the xml element to be added
                            text.Add(taskName.InnerText);
                            //the name of the xml element to be added
                            element.Add("weekly_or_monthly");
                            //the text inside the xml element to be added
                            text.Add(frequency.InnerText);
                            //the name of the xml element to be added
                            element.Add("completed");
                            //the text inside the xml element to be added
                            text.Add("false");
                            //the name of the xml element to be added
                            element.Add("date_completed");
                            //the text inside the xml element to be added
                            text.Add("");
                            //save each task as an xml element
                        }
                    }
                    else if (frequency.InnerText == "six_monthly")
                    {
                        DateTime startDate = DateTime.Now;
                        string date = xdoc.GetElementsByTagName("month")[i].ToString();
                           if  (date == startDate.AddMonths(6).ToString("MMMM")
                             || date == startDate.AddMonths(12).ToString("MMMM"))
                                                       
                        {
                            ArrayList element = new ArrayList();
                            ArrayList text = new ArrayList();
                            //the name of the xml element to be added
                            element.Add("task");
                            //the text inside the xml element to be added
                            text.Add(taskName.InnerText);
                            //the name of the xml element to be added
                            element.Add("weekly_or_monthly");
                            //the text inside the xml element to be added
                            text.Add(frequency.InnerText);
                            //the name of the xml element to be added
                            element.Add("completed");
                            //the text inside the xml element to be added
                            text.Add("false");
                            //the name of the xml element to be added
                            element.Add("date_completed");
                            //the text inside the xml element to be added
                            text.Add("");
                            //save each task as an xml element
                        }
                    }
                    else if (frequency.InnerText == "monthly")
                    {
                        ArrayList element = new ArrayList();
                        ArrayList text = new ArrayList();
                        //the name of the xml element to be added
                        element.Add("task");
                        //the text inside the xml element to be added
                        text.Add(taskName.InnerText);
                        //the name of the xml element to be added
                        element.Add("weekly_or_monthly");
                        //the text inside the xml element to be added
                        text.Add(frequency.InnerText);
                        //the name of the xml element to be added
                        element.Add("completed");
                        //the text inside the xml element to be added
                        text.Add("false");
                        //the name of the xml element to be added
                        element.Add("date_completed");
                        //the text inside the xml element to be added
                        text.Add("");
                        //save each task as an xml element


                        db.write(path + @"extra\" + DateTime.Now.ToString("MMMM-yyyy") + ".xml", element, text, "extra_tasks");
                    }
                    else { }

                    rfile.Close();
                }
            }
            catch (Exception err)
            {
                debugTerminal terminal = new debugTerminal();
                terminal.output(err.ToString());
            }
        }

        public int getMonthAsInt(string setMonth) {
            int getMonth;
            switch (setMonth)
            {
                case "January":
                    getMonth = 1;
                    break;
                case "Febuary":
                    getMonth = 2;
                    break;
                case "March":
                    getMonth = 3;
                    break;
                case "April":
                    getMonth = 4;
                    break;
                case "May":
                    getMonth = 5;
                    break;
                case "June":
                    getMonth = 6;
                    break;
                case "July":
                    getMonth = 7;
                    break;
                case "August":
                    getMonth = 8;
                    break;
                case "September":
                    getMonth = 9;
                    break;
                case "October":
                    getMonth = 10;
                    break;
                case "November":
                    getMonth = 11;
                    break;
                case "December":
                    getMonth = 12;
                    break;
                default:
                    getMonth = 0;
                    break;
            }
            return getMonth;
        }

        public string getMonthAsString(int setMonth)
        {
            string getMonth;
            switch (setMonth)
            {
                case 1:
                    getMonth = "January";
                    break;
                case 2:
                    getMonth = "Febuary";
                    break;
                case 3:
                    getMonth = "March";
                    break;
                case 4:
                    getMonth = "April";
                    break;
                case 5:
                    getMonth = "May";
                    break;
                case 6:
                    getMonth = "June";
                    break;
                case 7:
                    getMonth = "July";
                    break;
                case 8:
                    getMonth = "August";
                    break;
                case 9:
                    getMonth = "September";
                    break;
                case 10:
                    getMonth = "October";
                    break;
                case 11:
                    getMonth = "November";
                    break;
                case 12:
                    getMonth = "December";
                    break;
                default:
                    getMonth = "";
                    break;
            }
            return getMonth;
        }

        
        
       
    }
}